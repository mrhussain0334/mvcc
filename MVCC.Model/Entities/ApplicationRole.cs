﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationRole.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Newtonsoft.Json;

namespace MVCC.Model.Entities
{
    #region Usings

    using System;
    using Microsoft.AspNet.Identity.EntityFramework;

    #endregion Usings

    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public ApplicationRole()
        {
            this.Id = Guid.NewGuid();
        }

        public ApplicationRole(string name) : this()
        {
            this.Name = name;
        }
        [JsonIgnore]
        public RoleClaim Claims { get; set; }
    }
}