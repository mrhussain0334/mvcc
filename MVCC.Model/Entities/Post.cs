﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCC.Model.Entities
{
    public class Post
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int CreatedById { get; set; }
        [ForeignKey("CreatedById")]
        public ApplicationUser CreatedBy { get; set; }
        [Timestamp]
        public DateTime CreatedAt { get; set; }
        [Timestamp]
        public DateTime UpdatedAt { get; set; }
    }
}
