﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MVCC.Model.Entities
{
    public class Appointment : EntityBase
    {
        public DateTime AppointmentDate { get; set; }
        public DateTime AcceptedDate { get; set; }
        public bool IsApproved { get; set; }

        [JsonIgnore]
        public Guid DoctorId { get; set; }
        [ForeignKey("DoctorId")]
        [JsonIgnore]
        public virtual ApplicationUser Doctor { get; set; }
        [JsonIgnore]
        public Guid PatientId { get; set; }
        [ForeignKey("PatientId")]
        [JsonIgnore]
        public virtual ApplicationUser Patient { get; set; }

    }
}
