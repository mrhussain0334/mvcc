﻿// -----------------------------------------------------------------------
// <copyright file="Test.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCC.Model.Entities
{
    public class Test : EntityBase
    {
        public Guid? DoctorId { get; set; }
        [ForeignKey("DoctorId")]
        public virtual ApplicationUser Doctor { get; set; }
        public Guid? PatientId { get; set; }
        [ForeignKey("PatientId")]
        public virtual ApplicationUser Patient { get; set; }
        public DateTime TestDate { get; set; }
        //Orange = 0 || Green = 1
        public int Color { get; set; }
        public string Message { get; set; }
    }
}