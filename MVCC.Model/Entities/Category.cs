﻿using System;

namespace MVCC.Model.Entities
{
    public class Category : EntityBase
    {
        public string Name { get; set; }
        public Guid CreaterId { get; set; }
        public virtual ApplicationUser Creater { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }

    }
}
