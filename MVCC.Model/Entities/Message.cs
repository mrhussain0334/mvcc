﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCC.Model.Entities
{
    public class Message : EntityBase
    {
        public string Context { get; set; }
        public Guid FromId { get; set; }
        [ForeignKey("FromId")]
        public virtual ApplicationUser From { get; set; }
        public Guid ToId { get; set; }
        [ForeignKey("ToId")]
        public virtual ApplicationUser To { get; set; }
        public DateTime DateSent { get; set; }
        public DateTime DateRecieved { get; set; }
        public DateTime DateRead { get; set; }
        public bool IsRead { get; set; }
        public bool IsRecieved { get; set; }


    }
}
