﻿// -----------------------------------------------------------------------
// <copyright file="RoleClaim.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Model.Entities
{
    #region Usings

    using System;

    #endregion Usings

    [Flags]
    public enum RoleClaim
    {
        CanCreateUsers = 1,
        CanReadUsers = 2,
        CanUpdateUsers = 4,
        CanDeleteUsers = 8,
        CanCreateRoles = 16,
        CanReadRoles = 32,
        CanUpdateRoles = 64,
        CanDeleteRoles = 128,
        CanCreateTests = 256,
        CanReadTests = 512,
        CanUpdateTests = 1024,
        CanDeleteTests = 2048,
        CanCreateEvents = 4096,
        CanUpdateEvents = 8192,
        CanDeleteEvents = 16384,
        CanCreateCategory = 32768,
        CanDeleteCategory = 65536,
        CanApproveUser = 131072,
        CanMessageUser = 262144,
    }
}