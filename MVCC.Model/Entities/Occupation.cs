﻿namespace MVCC.Model.Entities
{
    public class Occupation : EntityBase
    {
        public string Name { get; set; }
    }
}
