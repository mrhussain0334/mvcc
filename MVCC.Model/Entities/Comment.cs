﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MVCC.Model.Entities
{
    public class Comment : EntityBase
    {
        public string Context { get; set; }

        [JsonIgnore]
        public Guid CommenterId { get; set; }
        [JsonIgnore]
        [ForeignKey("CommenterId")]
        public virtual ApplicationUser Commenter { get; set; }
        [JsonIgnore]
        public Guid DiscussionId { get; set; }
        [ForeignKey("DiscussionId")]
        [JsonIgnore]
        public virtual Discussion Discussion { get; set; }
        public DateTime CommentedDate { get; set; }

        public virtual string CommenterName => Commenter?.Name;

    }
}
