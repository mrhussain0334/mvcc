﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationUserLogin.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Model.Entities
{
    #region Usings

    using System;
    using Microsoft.AspNet.Identity.EntityFramework;

    #endregion Usings

    public class ApplicationUserLogin : IdentityUserLogin<Guid>
    {
    }
}