﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MVCC.Model.Entities
{
    public class Discussion : EntityBase
    {

        public string Name { get; set; }
        public string Description { get; set; }
        [JsonIgnore]
        public Guid CreaterId { get; set; }
        [JsonIgnore]
        public virtual ApplicationUser Creater { get; set; }
        public DateTime DateCreated { get; set; }
        [JsonIgnore]
        public DateTime DateUpdated { get; set; }
        public Guid CategoryId { get; set; }
        [JsonIgnore]
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public virtual string CreaterName => Creater?.Name;
    }
}
