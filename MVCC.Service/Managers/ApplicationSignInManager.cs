﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationSignInManager.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNet.Identity;

namespace MVCC.Service.Managers
{
    #region Usings

    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Model.Entities;

    #endregion Usings

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, Guid>
    {
        private readonly ApplicationRoleManager _roleManager;

        public ApplicationSignInManager(
            ApplicationUserManager userManager,
            IAuthenticationManager authenticationManager,
            ApplicationRoleManager roleManager) : base(userManager, authenticationManager)
        {
            this._roleManager = roleManager;
        }

        public override async Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            ClaimsIdentity ident = await user.GenerateUserIdentityAsync((ApplicationUserManager)this.UserManager);
            var userRoles = user.Roles.Select(r => r.RoleId);
            ident.AddClaims(
                this._roleManager.Roles.Where(r => userRoles.Any(ur => ur == r.Id))
                    .Select(r => r.Claims)
                    .ToList()
                    .Select(c => new Claim("RoleClaims", c.ToString())));
            return ident;
        }

        public async Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {

            PasswordHasher passwordHasher = new PasswordHasher();
            var user = await UserManager.FindAsync(userName, password);
            if (user != null && user.IsApproved)
            {
                
                return await base.PasswordSignInAsync(userName, password, isPersistent,shouldLockout);
            }
            else
            {
                return SignInStatus.Failure;
            }
        }
        public async Task<ApplicationUser> PasswordSignInAsync(string userName, string password,bool isApi)
        {

            var user = await UserManager.FindAsync(userName, password);
            if (user == null || !user.IsApproved) return null;
            return isApi ? user : null;
        }
    }
}