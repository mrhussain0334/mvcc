﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationRoleManager.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Service.Managers
{
    #region Usings

    using System;
    using Microsoft.AspNet.Identity;
    using Model.Entities;

    #endregion Usings

    public class ApplicationRoleManager : RoleManager<ApplicationRole, Guid>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole, Guid> store) : base(store)
        {
        }
    }
}