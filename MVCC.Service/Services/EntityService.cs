﻿// -----------------------------------------------------------------------
// <copyright file="EntityService.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Service.Services
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using Data.Infrastructure;
    using Interfaces;
    using Model.Entities;

    #endregion Usings

    public abstract class EntityService<T> : IEntityService<T> where T : EntityBase
    {
        private readonly IRepository<T> _repository;
        private readonly IUnitOfWork _unitOfWork;

        protected EntityService(IUnitOfWork unitOfWork, IRepository<T> repository)
        {
            this._unitOfWork = unitOfWork;
            this._repository = repository;
        }

        public virtual T Create(T entity)
        {
            T result = this._repository.Add(entity);
            this.Commit();
            return result;
        }

        public virtual void Delete(T entity)
        {
            this._repository.Delete(entity);
            this.Commit();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return this._repository.GetAll();
        }

        public T GetById(Guid Id)
        {
            return this._repository.GetById(Id);
        }

        public virtual void Update(T entity)
        {
            this._repository.Update(entity);
            this.Commit();
        }

        private void Commit()
        {
            this._unitOfWork.Commit();
        }
    }
}