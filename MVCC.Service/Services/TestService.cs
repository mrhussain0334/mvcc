﻿// -----------------------------------------------------------------------
// <copyright file="TestService.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Service.Services
{
    #region Usings

    using Data.Infrastructure;
    using Data.Interfaces;
    using Interfaces;
    using Model.Entities;

    #endregion Usings

    public class TestService : EntityService<Test>, ITestService
    {
        public TestService(IUnitOfWork unitOfWork, IRepository<Test> repository) : base(unitOfWork, repository)
        {
        }
    }
}