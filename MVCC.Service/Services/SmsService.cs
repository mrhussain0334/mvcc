﻿// -----------------------------------------------------------------------
// <copyright file="SmsService.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Service.Services
{
    #region Usings

    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;

    #endregion Usings

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}