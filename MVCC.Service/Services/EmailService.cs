﻿// -----------------------------------------------------------------------
// <copyright file="EmailService.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Service.Services
{
    #region Usings

    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;

    #endregion Usings

    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }
}