﻿// -----------------------------------------------------------------------
// <copyright file="ITestService.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Service.Interfaces
{
    #region Usings

    using Model.Entities;

    #endregion Usings

    public interface ITestService : IEntityService<Test>
    {
    }
}