﻿// -----------------------------------------------------------------------
// <copyright file="IEntityService.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Service.Interfaces
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using Model.Entities;

    #endregion Usings

    public interface IEntityService<T> where T : EntityBase
    {
        T Create(T entity);

        void Delete(T entity);

        IEnumerable<T> GetAll();

        T GetById(Guid Id);

        void Update(T entity);
    }
}