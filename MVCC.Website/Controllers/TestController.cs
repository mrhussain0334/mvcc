﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;
using MVCC.Service.Managers;
using MVCC.Website.Models;

namespace MVCC.Website.Controllers
{
    public class TestController : Controller
    {
        private readonly TestRepository _repository;
        private readonly ApplicationUserManager _userManager;
        public TestController(TestRepository repository, ApplicationUserManager userManager)
        {
            _repository = repository;
            _userManager = userManager;
        }


        // GET: Test
        public ActionResult Index()
        {
            var user = _userManager.FindByName(HttpContext.User.Identity.Name);
            var list = _repository.GetTest(user?.Id);
            var model = new TestIndexViewModel {Tests = list};
            return View(model);
        }

        [HttpGet]
        public ActionResult Create(Guid? patientId)
        {
            var model = new TestCreateViewModel();
            if(patientId != null)
                model.PatientId = (Guid) patientId;
            var user = _userManager.FindByName(HttpContext.User.Identity.Name);
            if (user == null) return RedirectToRoute("/");
            model.Patients = _userManager.Users.Where(m => m.AssignTo == user.Id).ToList();
            model.DoctorId = user.Id;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(TestCreateViewModel model)
        {
            var test = new Test()
            {
                Color = model.Color,
                DoctorId = model.DoctorId,
                PatientId = model.PatientId,
                Message = model.Message,
                TestDate = DateTime.Now
            };
            if (_repository.Add(test) != null)
            {
                TempData["Message"] = "Success";
                return RedirectToAction("Index");
            }
            TempData["Message"] = "Error";
            return RedirectToAction("Index");
        }





    }
}