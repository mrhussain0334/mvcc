﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVCC.Service.Managers;
using MVCC.Website.Models;

namespace MVCC.Website.Controllers
{
    public class PatientController : Controller
    {
        private readonly ApplicationUserManager _userManager;
        public PatientController(ApplicationUserManager manager)
        {
            _userManager = manager;
        }
        // GET: Patient
        public ActionResult Index()
        {
            var model = new PatientViewModel();
            var user = _userManager.FindByName(HttpContext.User.Identity.Name);
            if (user != null)
            {
                model.Patients = _userManager.Users.Where(m => m.AssignTo == user.Id).ToList();
                return View(model);
            }
            TempData["Message"] = "Error";
            return Redirect("/");
        }


    }
}