﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVCC.Model.Entities;
using MVCC.Service.Managers;
using MVCC.Website.Attributes;

namespace MVCC.Website.Controllers
{
    public class ApproveController : Controller
    {
        private readonly ApplicationRoleManager _roleManager;

        private readonly ApplicationUserManager _userManager;

        public ApproveController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanDeleteUsers)]

        // POST: /Approve/Delete/5
        [HttpGet]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            if (this.ModelState.IsValid)
            {
                if (id != Guid.Empty)
                {
                    ApplicationUser user = await this._userManager.FindByIdAsync(id);
                    if (user == null)
                    {
                        return this.HttpNotFound();
                    }
                    IdentityResult result = await this._userManager.DeleteAsync(user);
                    if (!result.Succeeded)
                    {
                        this.ModelState.AddModelError("", result.Errors.First());
                        return this.View();
                    }
                    return this.RedirectToAction("Index");
                }
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return this.View();
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanApproveUser)]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return this.View(await this._userManager.Users.Where(u => !u.IsApproved).ToListAsync());
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanApproveUser)]
        [HttpGet]
        public async Task<ActionResult> Approve(Guid id)
        {
            var user = await this._userManager.FindByIdAsync(id);
            if (user != null)
            {
                user.IsApproved = true;
                var result = _userManager.Update(user);
            }
            return this.RedirectToAction("Index");
        }
    }
}