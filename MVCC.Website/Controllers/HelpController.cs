﻿// -----------------------------------------------------------------------
// <copyright file="HelpController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System.Web.Mvc;

    #endregion Usings

    [AllowAnonymous]
    public class HelpController : Controller
    {
        public ActionResult FAQ()
        {
            this.ViewBag.Title = "FAQ's for Patients and Visitors by Patients and Visitors";
            return this.View();
        }

        public ActionResult HowToChangeAppointment()
        {
            this.ViewBag.Title = "How to Change your Appointment";
            return this.View();
        }

        // GET: Help
        public ActionResult Index()
        {
            this.ViewBag.Title = "Where to go for Help/FAQ's";
            return this.View();
        }

        public ActionResult LyndaJackson()
        {
            this.ViewBag.Title = "Lynda Jackson Macmillan Centre";
            return this.View();
        }

        public ActionResult OutOfHours()
        {
            this.ViewBag.Title = "What to do Out of Hours";
            return this.View();
        }

        public ActionResult UsefulLinks()
        {
            this.ViewBag.Title = "Useful Links";
            return this.View();
        }

        public ActionResult VideoDiaries()
        {
            this.ViewBag.Title = "Patient Video Diaries";
            return this.View();
        }
    }
}