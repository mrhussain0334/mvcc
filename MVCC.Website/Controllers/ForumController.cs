﻿using System;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;
using MVCC.Web.Shared.Extensions;
using MVCC.Website.Models;
using PagedList;
namespace MVCC.Website.Controllers
{
    public class ForumController : Controller
    {
        private readonly CategoryRepository _repository;
        private readonly DiscussionRepository _discussionRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly CommentRepository _commentRepository;
        public ForumController(CategoryRepository repository, DiscussionRepository discussionRepository, CategoryRepository categoryRepository, CommentRepository commentRepository)
        {
            _repository = repository;
            _discussionRepository = discussionRepository;
            _categoryRepository = categoryRepository;
            _commentRepository = commentRepository;
        }
        // GET: Forum
        public ActionResult Index(int? page)
        {
            page = page ?? 1;

            var model = new ForumIndexViewModel();
            var list = _repository.GetAll();
            var temp = list.Select(category => new ForumIndexViewModel.Model()
            {
                Category = category, Count = _discussionRepository.CountByCategory(category)
            }).ToList();
            model.Models = temp.ToPagedList(page.Value,30);
            return View(model);
        }


        public ActionResult ForumCategory(Guid id,int? page)
        {
            var model = new ForumCategoryViewModel
            {
                PageNumber = page ?? 1,
                PageCount = _discussionRepository.Pages(30)
            };
            model.Discussions = _discussionRepository.GetByCategory(id).ToPagedList(model.PageNumber,30);
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new ForumCreateViewModel()
            {
                Categories = _categoryRepository.GetAll()
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(ForumCreateViewModel model)
        {
            var discussion = new Discussion()
            {
                CategoryId = model.CategoryId,
                CreaterId = User.Identity.GetGuidUserId(),
                DateCreated = DateTime.Now,
                DateUpdated = DateTime.Now,
                Description = model.Description,
                Name = model.Name
            };
            var disc = _discussionRepository.Add(discussion);
            if (disc != null)
            {
                TempData["Message"] = "Success";
                return RedirectToAction("Discussion",new { id = disc.Id});
            }
            TempData["Message"] = "Error";
            return View(model);
        }

        public ActionResult Discussion(Guid id,int? page)
        {
            var discussion = _discussionRepository.GetById(id);
            if (discussion == null)
            {
                TempData["Message"] = "Error";
                return RedirectToAction("Index");
            }

            var model = new DiscussionViewModel
            {
                Id = id,
                Page = page ?? 1,
                Name = discussion.Name,
                Description = discussion.Description,
                CreaterName = discussion.Creater.Name,
                DateCreated = discussion.DateCreated,
                Category = discussion.Category.Name,
                PageCount = _commentRepository.GetCountByDiscussion(id, 20),
            };
            model.Comments = _commentRepository.GetByDiscussion(id, model.Page, 20);
            return View(model);

        }

        public ActionResult Edit(Guid id)
        {
            var discussion = _discussionRepository.GetById(id);
            if (discussion.CreaterId.ToString() == HttpContext.User.Identity.GetUserId() || HttpContext.User.IsInRole("Admin"))
            {
                var model = new DiscussionEditViewModel()
                {
                    Id = id,
                    CategoryId = discussion.CategoryId,
                    Name = discussion.Name,
                    Description = discussion.Description,
                    Categories = _categoryRepository.GetAll()
                };
                return View(model);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Update(DiscussionEditViewModel model)
        {
            var discussion = new Discussion()
            {
                Id = model.Id,
                CategoryId = model.CategoryId,
                Name = model.Name,
                Description = model.Description
            };

            var check = _discussionRepository.Update(discussion);
            if (check)
            {
                TempData["Message"] = "Success";
                return RedirectToAction("Index", new {id = model.Id});
            }
            TempData["Message"] = "Failed";
            return RedirectToAction("Discussions","Profile");
        }

        public ActionResult Delete(Guid id)
        {
            var discussion = _discussionRepository.GetById(id);
            if (discussion.CreaterId.ToString() == HttpContext.User.Identity.GetUserId() || HttpContext.User.IsInRole("Admin"))
            {
                _discussionRepository.Delete(discussion);
                TempData["Message"] = "Success";
                return RedirectToAction("Discussion","Profile");
            }
            TempData["Message"] = "Error";
            return RedirectToAction("Discussion","Profile");

        }

        [HttpPost]
        public ActionResult Comment(CommentCreateViewModel model)
        {
            var comment = new Comment()
            {
                Context = model.Context,
                CommentedDate = DateTime.Now,
                CommenterId = User.Identity.GetGuidUserId(),
                DiscussionId = model.DiscussionId
            };

            var com = _commentRepository.Add(comment);
            if (com != null)
            {
                return RedirectToAction("Discussion",
                    new
                    {
                        id = model.DiscussionId,
                        page = _commentRepository.GetCountByDiscussion(model.DiscussionId, 20)
                    });
            }
            TempData["Message"] = "Error";
            return RedirectToAction("Discussion", new {id = model.DiscussionId});
        }

    }
}