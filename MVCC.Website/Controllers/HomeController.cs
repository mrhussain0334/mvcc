﻿// -----------------------------------------------------------------------
// <copyright file="HomeController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System.Web.Mvc;

    #endregion Usings

    [AllowAnonymous]
    public class HomeController : Controller
    {
        public ActionResult Education()
        {
            return this.RedirectToAction("Index", "Education");
        }

        public ActionResult GetInvolved()
        {
            return this.RedirectToAction("Index", "Involved");
        }

        public ActionResult Help()
        {
            return this.RedirectToAction("Index", "Help");
        }

        public ActionResult Index()
        {
            this.ViewBag.Title = "Mount Vernon Cancer Centre";

            return this.View();
        }

        public ActionResult Supporting()
        {
            return this.RedirectToAction("Index", "Supporting");
        }

        public ActionResult Treatments()
        {
            return this.RedirectToAction("Index", "Treatments");
        }

        public ActionResult Visiting()
        {
            return this.RedirectToAction("Index", "Visiting");
        }
    }
}