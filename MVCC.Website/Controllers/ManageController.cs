﻿// -----------------------------------------------------------------------
// <copyright file="ManageController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Model.Entities;
    using Models;
    using Service.Managers;
    using Web.Shared.Extensions;

    #endregion Usings

    [Authorize]
    public class ManageController : Controller
    {
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private readonly IAuthenticationManager _authenticationManager;

        private readonly ApplicationSignInManager _signInManager;

        private readonly ApplicationUserManager _userManager;

        public ManageController(
            ApplicationUserManager userManager,
            ApplicationSignInManager signInManager,
            IAuthenticationManager authenticationManager)
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._authenticationManager = authenticationManager;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return this.View();
        }

        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            // Generate the token and send it
            string code = await this._userManager.GenerateChangePhoneNumberTokenAsync(this.User.Identity.GetGuidUserId(), model.Number);
            if (this._userManager.SmsService != null)
            {
                IdentityMessage message = new IdentityMessage { Destination = model.Number, Body = "Your security code is: " + code };
                await this._userManager.SmsService.SendAsync(message);
            }
            return this.RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return this.View();
        }

        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }
            IdentityResult result = await this._userManager.ChangePasswordAsync(
                this.User.Identity.GetGuidUserId(),
                model.OldPassword,
                model.NewPassword);
            if (result.Succeeded)
            {
                ApplicationUser user = await this._userManager.FindByIdAsync(this.User.Identity.GetGuidUserId());
                if (user != null)
                {
                    await this._signInManager.SignInAsync(user, false, false);
                }
                return this.RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            this.AddErrors(result);
            return this.View(model);
        }

        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await this._userManager.SetTwoFactorEnabledAsync(this.User.Identity.GetGuidUserId(), false);
            ApplicationUser user = await this._userManager.FindByIdAsync(this.User.Identity.GetGuidUserId());
            if (user != null)
            {
                await this._signInManager.SignInAsync(user, false, false);
            }
            return this.RedirectToAction("Index", "Manage");
        }

        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await this._userManager.SetTwoFactorEnabledAsync(this.User.Identity.GetGuidUserId(), true);
            ApplicationUser user = await this._userManager.FindByIdAsync(this.User.Identity.GetGuidUserId());
            if (user != null)
            {
                await this._signInManager.SignInAsync(user, false, false);
            }
            return this.RedirectToAction("Index", "Manage");
        }

        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            this.ViewBag.StatusMessage = message == ManageMessageId.ChangePasswordSuccess
                ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess
                    ? "Your password has been set."
                    : message == ManageMessageId.SetTwoFactorSuccess
                        ? "Your two-factor authentication provider has been set."
                        : message == ManageMessageId.Error
                            ? "An error has occurred."
                            : message == ManageMessageId.AddPhoneSuccess
                                ? "Your phone number was added."
                                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed." : "";

            Guid userId = this.User.Identity.GetGuidUserId();
            IndexViewModel model = new IndexViewModel
            {
                HasPassword = this.HasPassword(),
                PhoneNumber = await this._userManager.GetPhoneNumberAsync(userId),
                TwoFactor = await this._userManager.GetTwoFactorEnabledAsync(userId),
                Logins = await this._userManager.GetLoginsAsync(userId),
                BrowserRemembered = await this._authenticationManager.TwoFactorBrowserRememberedAsync(userId.ToString())
            };
            return this.View(model);
        }

        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(
                provider,
                this.Url.Action("LinkLoginCallback", "Manage"),
                this.User.Identity.GetUserId());
        }

        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            ExternalLoginInfo loginInfo = await this._authenticationManager.GetExternalLoginInfoAsync(XsrfKey, this.User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return this.RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            IdentityResult result = await this._userManager.AddLoginAsync(this.User.Identity.GetGuidUserId(), loginInfo.Login);
            return result.Succeeded
                ? this.RedirectToAction("ManageLogins")
                : this.RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            this.ViewBag.StatusMessage = message == ManageMessageId.RemoveLoginSuccess
                ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred." : "";
            ApplicationUser user = await this._userManager.FindByIdAsync(this.User.Identity.GetGuidUserId());
            if (user == null)
            {
                return this.View("Error");
            }
            var userLogins = await this._userManager.GetLoginsAsync(this.User.Identity.GetGuidUserId());
            var otherLogins =
                this._authenticationManager.GetExternalAuthenticationTypes()
                    .Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider))
                    .ToList();
            this.ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return this.View(new ManageLoginsViewModel { CurrentLogins = userLogins, OtherLogins = otherLogins });
        }

        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            IdentityResult result = await this._userManager.RemoveLoginAsync(
                this.User.Identity.GetGuidUserId(),
                new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                ApplicationUser user = await this._userManager.FindByIdAsync(this.User.Identity.GetGuidUserId());
                if (user != null)
                {
                    await this._signInManager.SignInAsync(user, false, false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return this.RedirectToAction("ManageLogins", new { Message = message });
        }

        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            IdentityResult result = await this._userManager.SetPhoneNumberAsync(this.User.Identity.GetGuidUserId(), null);
            if (!result.Succeeded)
            {
                return this.RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            ApplicationUser user = await this._userManager.FindByIdAsync(this.User.Identity.GetGuidUserId());
            if (user != null)
            {
                await this._signInManager.SignInAsync(user, false, false);
            }
            return this.RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return this.View();
        }

        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                IdentityResult result = await this._userManager.AddPasswordAsync(this.User.Identity.GetGuidUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    ApplicationUser user = await this._userManager.FindByIdAsync(this.User.Identity.GetGuidUserId());
                    if (user != null)
                    {
                        await this._signInManager.SignInAsync(user, false, false);
                    }
                    return this.RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                this.AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return this.View(model);
        }

        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            string code = await this._userManager.GenerateChangePhoneNumberTokenAsync(this.User.Identity.GetGuidUserId(), phoneNumber);

            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? this.View("Error") : this.View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }
            IdentityResult result = await this._userManager.ChangePhoneNumberAsync(
                this.User.Identity.GetGuidUserId(),
                model.PhoneNumber,
                model.Code);
            if (result.Succeeded)
            {
                ApplicationUser user = await this._userManager.FindByIdAsync(this.User.Identity.GetGuidUserId());
                if (user != null)
                {
                    await this._signInManager.SignInAsync(user, false, false);
                }
                return this.RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }

            // If we got this far, something failed, redisplay form
            this.ModelState.AddModelError("", "Failed to verify phone");
            return this.View(model);
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                this.ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            ApplicationUser user = this._userManager.FindById(this.User.Identity.GetGuidUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            ApplicationUser user = this._userManager.FindById(this.User.Identity.GetGuidUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        #endregion Helpers
    }
}