﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCC.Data.Repositories;
using MVCC.Web.Shared.Extensions;
using MVCC.Website.Models;
using PagedList;

namespace MVCC.Website.Controllers
{
    public class ProfileController : Controller
    {

        private readonly DiscussionRepository _discussionRepository;
        public ProfileController(DiscussionRepository discussionRepository)
        {
            _discussionRepository = discussionRepository;
        }
        // GET: Profile
        public ActionResult Discussion(int? page)
        {
            var Page = page ?? 1;
            var list = _discussionRepository.GetByUser(HttpContext.User.Identity.GetGuidUserId()).ToPagedList(Page,20);
            var model = new ProfileDiscussionViewModel {Discussions = list};
            return View(model);
        }
    }
}