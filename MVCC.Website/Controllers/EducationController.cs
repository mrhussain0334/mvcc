﻿// -----------------------------------------------------------------------
// <copyright file="EducationController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System.Web.Mvc;

    #endregion Usings

    [AllowAnonymous]
    public class EducationController : Controller
    {
        // GET: Education
        public ActionResult Index()
        {
            this.ViewBag.Title = "Mount Vernon Cancer Education Centre";
            return this.View();
        }
    }
}