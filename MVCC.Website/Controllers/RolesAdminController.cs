﻿// -----------------------------------------------------------------------
// <copyright file="RolesAdminController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Attributes;
    using Microsoft.AspNet.Identity;
    using Model.Entities;
    using Models;
    using Service.Managers;

    #endregion Usings

    public class RolesAdminController : Controller
    {
        private readonly ApplicationRoleManager _roleManager;

        private readonly ApplicationUserManager _userManager;

        public RolesAdminController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanCreateRoles)]

        // GET: /Roles/Create
        public ActionResult Create()
        {
            this.ViewBag.ClaimId =
                new SelectList(
                    Enum.GetValues(typeof(RoleClaim))
                        .Cast<RoleClaim>()
                        .Select(v => new SelectListItem { Text = v.ToString(), Value = ((int)v).ToString() })
                        .ToList(),
                    "Value",
                    "Text");
            return this.View();
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanCreateRoles)]

        // POST: /Roles/Create
        [HttpPost]
        public async Task<ActionResult> Create(RoleViewModel roleViewModel, params int[] selectedClaims)
        {
            if (this.ModelState.IsValid)
            {
                // Use ApplicationRole, not IdentityRole:
                ApplicationRole role = new ApplicationRole(roleViewModel.Name);
                if (selectedClaims != null)
                {
                    role.Claims = (RoleClaim)selectedClaims.Sum();
                }
                IdentityResult roleresult = await this._roleManager.CreateAsync(role);
                if (!roleresult.Succeeded)
                {
                    this.ModelState.AddModelError("", roleresult.Errors.First());
                    return this.View();
                }
                return this.RedirectToAction("Index");
            }
            return this.View();
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanDeleteRoles)]

        // GET: /Roles/Delete/5
        public async Task<ActionResult> Delete(Guid id)
        {
            if (id != Guid.Empty)
            {
                ApplicationRole role = await this._roleManager.FindByIdAsync(id);
                if (role == null)
                {
                    return this.HttpNotFound();
                }
                return this.View(role);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanDeleteRoles)]

        // POST: /Roles/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id, string deleteUser)
        {
            if (this.ModelState.IsValid)
            {
                if (id != Guid.Empty)
                {
                    ApplicationRole role = await this._roleManager.FindByIdAsync(id);
                    if (role == null)
                    {
                        return this.HttpNotFound();
                    }
                    IdentityResult result;
                    if (deleteUser != null)
                    {
                        result = await this._roleManager.DeleteAsync(role);
                    }
                    else
                    {
                        result = await this._roleManager.DeleteAsync(role);
                    }
                    if (!result.Succeeded)
                    {
                        this.ModelState.AddModelError("", result.Errors.First());
                        return this.View();
                    }
                    return this.RedirectToAction("Index");
                }
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return this.View();
        }

        [RoleClaimAuthorize(
                RoleClaim =
                    RoleClaim.CanCreateRoles | RoleClaim.CanReadRoles | RoleClaim.CanDeleteRoles | RoleClaim.CanUpdateRoles)
        ]

        // GET: /Roles/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            if (id != Guid.Empty)
            {
                ApplicationRole role = await this._roleManager.FindByIdAsync(id);

                // Get the list of Users in this Role
                var users = new List<ApplicationUser>();

                // Get the list of Users in this Role
                foreach (ApplicationUser user in this._userManager.Users.ToList())
                {
                    if (await this._userManager.IsInRoleAsync(user.Id, role.Name))
                    {
                        users.Add(user);
                    }
                }

                var claims = new List<RoleClaim>();
                foreach (RoleClaim value in Enum.GetValues(typeof(RoleClaim)))
                {
                    if (role.Claims.HasFlag(value))
                    {
                        claims.Add(value);
                    }
                }

                this.ViewBag.Users = users;
                this.ViewBag.UserCount = users.Count;
                this.ViewBag.Claims = claims;
                this.ViewBag.ClaimCount = claims.Count;
                return this.View(role);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanUpdateRoles)]

        // GET: /Roles/Edit/Admin
        public async Task<ActionResult> Edit(Guid id)
        {
            if (id != Guid.Empty)
            {
                ApplicationRole role = await this._roleManager.FindByIdAsync(id);
                if (role == null)
                {
                    return this.HttpNotFound();
                }
                RoleViewModel roleModel = new RoleViewModel
                {
                    Id = role.Id,
                    Name = role.Name,
                    ClaimsList =
                        Enum.GetValues(typeof(RoleClaim))
                            .Cast<RoleClaim>()
                            .Select(
                                v =>
                                    new SelectListItem
                                    {
                                        Selected = role.Claims.HasFlag(v),
                                        Text = v.ToString(),
                                        Value = ((int)v).ToString()
                                    })
                            .ToList()
                };
                return this.View(roleModel);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanUpdateRoles)]

        // POST: /Roles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(Include = "Name,Id")] RoleViewModel roleModel,
            params int[] selectedClaim)
        {
            if (this.ModelState.IsValid)
            {
                ApplicationRole role = await this._roleManager.FindByIdAsync(roleModel.Id);
                role.Name = roleModel.Name;
                role.Claims = (RoleClaim)selectedClaim.Sum();
                await this._roleManager.UpdateAsync(role);
                return this.RedirectToAction("Index");
            }
            return this.View();
        }

        [RoleClaimAuthorize(
                RoleClaim =
                    RoleClaim.CanCreateRoles | RoleClaim.CanReadRoles | RoleClaim.CanDeleteRoles | RoleClaim.CanUpdateRoles)
        ]

        // GET: /Roles/
        public ActionResult Index()
        {
            return this.View(this._roleManager.Roles);
        }
    }
}