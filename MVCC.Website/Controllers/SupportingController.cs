﻿// -----------------------------------------------------------------------
// <copyright file="SupportingController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System.Web.Mvc;

    #endregion Usings

    [AllowAnonymous]
    public class SupportingController : Controller
    {
        // GET: Supporting
        public ActionResult Index()
        {
            this.ViewBag.Title = "Supporting Someone with Cancer";
            return this.View();
        }
    }
}