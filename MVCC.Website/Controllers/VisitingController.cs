﻿// -----------------------------------------------------------------------
// <copyright file="VisitingController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System.Web.Mvc;

    #endregion Usings

    [AllowAnonymous]
    public class VisitingController : Controller
    {
        public ActionResult CarParking()
        {
            this.ViewBag.Title = "Car Parking";
            return this.View();
        }

        public ActionResult DirectionsAndTravel()
        {
            this.ViewBag.Title = "Directions and Travel";
            return this.View();
        }

        // GET: Visiting
        public ActionResult Index()
        {
            this.ViewBag.Title = "Visiting Mount Vernon Cancer Centre";
            return this.View();
        }

        public ActionResult Partners()
        {
            this.ViewBag.Title = "Our Partners on Site";
            return this.View();
        }

        public ActionResult Site()
        {
            this.ViewBag.Title = "MVCC Site";
            return this.View();
        }

        public ActionResult VisitorFacilities()
        {
            this.ViewBag.Title = "Visitor Services and Facilities";
            return this.View();
        }

        public ActionResult YourVisit()
        {
            this.ViewBag.Title = "Your Visit";
            return this.View();
        }
    }
}