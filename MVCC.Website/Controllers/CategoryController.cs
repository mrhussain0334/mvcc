﻿using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;
using MVCC.Service.Managers;
using MVCC.Website.Attributes;
using MVCC.Website.Models;

namespace MVCC.Website.Controllers
{
    public class CategoryController : Controller
    {
        private readonly CategoryRepository _repository;
        private readonly ApplicationUserManager _manager;
        public CategoryController(CategoryRepository repository, ApplicationUserManager manager)
        {
            _repository = repository;
            _manager = manager;
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanCreateCategory)]
        public ActionResult Index()
        {
            var model = new CategoryIndexViewModel {Categories = _repository.GetAll()};
            return View(model);
        }


        [HttpGet]
        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanCreateCategory)]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanCreateCategory)]
        public ActionResult Create(CategoryCreateViewModel model)
        {
            var user = _manager.FindByName(HttpContext.User.Identity.Name);
            if (user == null) return RedirectToAction("Index");
            var cat = new Category {Name = model.CategoryName , CreaterId = user.Id};
            cat.DateCreated = cat.DateUpdated = DateTime.Now;
            if (_repository.Add(cat) != null)
            {
                TempData["Message"] = "Success";
            }
            else
            {
                TempData["Message"] = "Error";
            }
            return RedirectToAction("Index");
        }



        [HttpGet]
        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanDeleteCategory)]
        public ActionResult Update(Guid id)
        {
            var cat = _repository.GetById(id);
            if (cat == null) return RedirectToAction("Index");
            var model = new CategoryUpdateViewModel()
            {
                
                Category = cat
            };

            return View(model);
        }


        [HttpGet]
        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanDeleteCategory)]
        public ActionResult Delete(Guid id)
        {
            var cat = _repository.GetById(id);
            if (cat == null) return RedirectToAction("Index");
            _repository.Delete(cat);
            TempData["Message"] = "Category Deleted";
            return RedirectToAction("Index");
        }


        [HttpPost]
        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanDeleteCategory)]
        public ActionResult Update(Category model)
        {
            if (_repository.Update(model))
            {
                TempData["Message"] = "Success";
            }
            else
            {
                TempData["Message"] = "Error";
            }
            return RedirectToAction("Index");
        }




    }
}