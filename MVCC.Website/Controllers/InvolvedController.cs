﻿// -----------------------------------------------------------------------
// <copyright file="InvolvedController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System.Web.Mvc;

    #endregion Usings

    [AllowAnonymous]
    public class InvolvedController : Controller
    {
        public ActionResult Charity()
        {
            this.ViewBag.Title = "Mount Vernon Cancer Centre Charity";
            return this.View();
        }

        public ActionResult ContactUs()
        {
            this.ViewBag.Title = "Contact Us";
            return this.View();
        }

        public ActionResult Feedback()
        {
            this.ViewBag.Title = "Give your Feedback";
            return this.View();
        }

        // GET: Involved
        public ActionResult Index()
        {
            this.ViewBag.Title = "Get Involved";
            return this.View();
        }

        public ActionResult NewsAndEvents()
        {
            this.ViewBag.Title = "News and Events";
            return this.View();
        }

        public ActionResult PastEvents()
        {
            this.ViewBag.Title = "Past Events";
            return this.View();
        }

        public ActionResult Volunteering()
        {
            this.ViewBag.Title = "Volunteering";
            return this.View();
        }
    }
}