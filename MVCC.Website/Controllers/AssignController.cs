﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MVCC.Model.Entities;
using MVCC.Service.Managers;
using MVCC.Website.Attributes;
using MVCC.Website.Models;

namespace MVCC.Website.Controllers
{
    public class AssignController : Controller
    {

        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;

        public AssignController(ApplicationUserManager user, ApplicationRoleManager roleManager)
        {
            _userManager = user;
            _roleManager = roleManager;
        }
        // GET: Assign
        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanApproveUser)]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var role = await _roleManager.FindByNameAsync("Patient");
            if (role == null) return View("Index");
            var rolelist = role.Users.Select(r => r.UserId);
            var list = _userManager.Users.Where(u => u.Roles.Any(r => rolelist.Contains(r.UserId))).ToList();
            var model = new AssignIndexViewModel();
            if (list.Count > 0)
            {
                model.PatientsList = list;
            }
            
            return View("Index", model);
        }


        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanApproveUser)]
        [HttpGet]
        public async Task<ActionResult> Assign(Guid id)
        {
            var p = await _userManager.FindByIdAsync(id);
            var role = await _roleManager.FindByNameAsync("Doctor");
            if (role == null) return View("Index");
            var rolelist = role.Users.Select(r => r.UserId);
            var list = _userManager.Users.Where(u => u.Roles.Any(r => rolelist.Contains(r.UserId))).ToList();
            var Model = new AssignPatientViewModel()
            {
                Patient = p,
            };
            if (list.Count > 0)
            {
                Model.DoctorsList = list;
            }
            return View("Assign", Model);
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanApproveUser)]
        [HttpPost]
        public async Task<ActionResult> Assign(Guid patientId,Guid doctorId)
        {

            var p = await _userManager.FindByIdAsync(patientId);
            if(p != null) { 
                var d = await _userManager.FindByIdAsync(doctorId);
                if (d != null)
                {
                    p.AssignTo = d.Id;
                    var result = await _userManager.UpdateAsync(p);
                    if (!result.Succeeded)
                    {
                        TempData["Message"] = "Error When Assigning Patient";
                        return RedirectToAction("Index");
                    }
                }
            }
            TempData["Message"] = "Success";
            return RedirectToAction("Index");
        }



    }
}