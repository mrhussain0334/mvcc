﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVCC.Model.Entities;
using MVCC.Service.Managers;

namespace MVCC.Website.Controllers
{
    public class CustomController : Controller
    {
        private readonly ApplicationUserManager _userManager;

        public CustomController(ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        public ActionResult MyPatients()
        {
            try
            {
                var id = HttpContext.User.Identity.GetUserId();
                var list = _userManager.Users.Where(m => m.AssignTo.ToString() == id).ToList();
                return Json(list,JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                
            }
            return null;
        }
    }
}
