﻿// -----------------------------------------------------------------------
// <copyright file="UsersAdminController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Attributes;
    using Microsoft.AspNet.Identity;
    using Model.Entities;
    using Models;
    using Service.Managers;

    #endregion Usings

    public class UsersAdminController : Controller
    {
        private readonly ApplicationRoleManager _roleManager;

        private readonly ApplicationUserManager _userManager;

        public UsersAdminController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
        }


        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanApproveUser)]
        [HttpGet]
        public async Task<ActionResult> Approve()
        {
            return this.View(await this._userManager.Users.Where(u => !u.IsApproved).ToListAsync());
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanApproveUser)]
        [HttpPost]
        public async Task<ActionResult> Approve(Guid id)
        {
            var user = await this._userManager.FindByIdAsync(id);
            if (user != null)
            {
                user.IsApproved = true;
                var result = await _userManager.UpdateAsync(user);
            }
            return this.RedirectToAction("Approve");
        }


        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanCreateUsers)]

        // GET: /Users/Create
        public async Task<ActionResult> Create()
        {
            // Get the list of Roles
            this.ViewBag.RoleId = new SelectList(await this._roleManager.Roles.ToListAsync(), "Name", "Name");
            return this.View();
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanCreateUsers)]

        // POST: /Users/Create
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            if (this.ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser { UserName = userViewModel.Email, Email = userViewModel.Email };
                IdentityResult adminresult = await this._userManager.CreateAsync(user, userViewModel.Password);

                // Add User to the selected Roles
                if (adminresult.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        IdentityResult result = await this._userManager.AddToRolesAsync(user.Id, selectedRoles);
                        if (!result.Succeeded)
                        {
                            this.ModelState.AddModelError("", result.Errors.First());
                            this.ViewBag.RoleId = new SelectList(await this._roleManager.Roles.ToListAsync(), "Name", "Name");
                            return this.View();
                        }
                    }
                }
                else
                {
                    this.ModelState.AddModelError("", adminresult.Errors.First());
                    this.ViewBag.RoleId = new SelectList(this._roleManager.Roles, "Name", "Name");
                    return this.View();
                }
                return this.RedirectToAction("Index");
            }
            this.ViewBag.RoleId = new SelectList(this._roleManager.Roles, "Name", "Name");
            return this.View();
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanDeleteUsers)]

        // GET: /Users/Delete/5
        public async Task<ActionResult> Delete(Guid id)
        {
            if (id != Guid.Empty)
            {
                ApplicationUser user = await this._userManager.FindByIdAsync(id);
                if (user == null)
                {
                    return this.HttpNotFound();
                }
                return this.View(user);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanDeleteUsers)]

        // POST: /Users/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            if (this.ModelState.IsValid)
            {
                if (id != Guid.Empty)
                {
                    ApplicationUser user = await this._userManager.FindByIdAsync(id);
                    if (user == null)
                    {
                        return this.HttpNotFound();
                    }
                    IdentityResult result = await this._userManager.DeleteAsync(user);
                    if (!result.Succeeded)
                    {
                        this.ModelState.AddModelError("", result.Errors.First());
                        return this.View();
                    }
                    return this.RedirectToAction("Index");
                }
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return this.View();
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanCreateUsers | RoleClaim.CanReadUsers | RoleClaim.CanUpdateUsers | RoleClaim.CanDeleteUsers)]

        // GET: /Users/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            if (id != Guid.Empty)
            {
                // Process normally:
                ApplicationUser user = await this._userManager.FindByIdAsync(id);
                this.ViewBag.RoleNames = await this._userManager.GetRolesAsync(user.Id);
                return this.View(user);
            }

            // Return Error:
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanUpdateUsers)]

        // GET: /Users/Edit/1
        public async Task<ActionResult> Edit(Guid id)
        {
            if (id != Guid.Empty)
            {
                ApplicationUser user = await this._userManager.FindByIdAsync(id);
                if (user == null)
                {
                    return this.HttpNotFound();
                }

                var userRoles = await this._userManager.GetRolesAsync(user.Id);
                return
                    this.View(
                        new EditUserViewModel
                        {
                            Id = user.Id,
                            Email = user.Email,
                            RolesList =
                                this._roleManager.Roles.ToList()
                                    .Select(x => new SelectListItem { Selected = userRoles.Contains(x.Name), Text = x.Name, Value = x.Name })
                        });
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanUpdateUsers)]

        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Email,Id")] EditUserViewModel editUser, params string[] selectedRole)
        {
            if (this.ModelState.IsValid)
            {
                ApplicationUser user = await this._userManager.FindByIdAsync(editUser.Id);
                if (user == null)
                {
                    return this.HttpNotFound();
                }

                user.UserName = editUser.Email;
                user.Email = editUser.Email;

                var userRoles = await this._userManager.GetRolesAsync(user.Id);

                selectedRole = selectedRole ?? new string[] { };

                IdentityResult result = await this._userManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray());

                if (!result.Succeeded)
                {
                    this.ModelState.AddModelError("", result.Errors.First());
                    return this.View();
                }
                result = await this._userManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray());

                if (!result.Succeeded)
                {
                    this.ModelState.AddModelError("", result.Errors.First());
                    return this.View();
                }
                return this.RedirectToAction("Index");
            }
            this.ModelState.AddModelError("", "Something failed.");
            return this.View();
        }

        [RoleClaimAuthorize(RoleClaim = RoleClaim.CanCreateUsers | RoleClaim.CanReadUsers | RoleClaim.CanUpdateUsers | RoleClaim.CanDeleteUsers)]

        // GET: /Users/
        public async Task<ActionResult> Index()
        {
            return this.View(await this._userManager.Users.ToListAsync());
        }
    }
}