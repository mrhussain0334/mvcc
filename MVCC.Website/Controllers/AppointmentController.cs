﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCC.Data.Repositories;
using MVCC.Web.Shared.Extensions;
using MVCC.Website.Models;
using PagedList;

namespace MVCC.Website.Controllers
{
    public class AppointmentController : Controller
    {
        private AppointmentRepository _appointmentRepository;

        public AppointmentController(AppointmentRepository appointmentRepository)
        {
            _appointmentRepository = appointmentRepository;
        }

        // GET: Appointment
        public ActionResult Index(int page = 1)
        {


            var model = new AppointmentViewModel();
            var list =
                _appointmentRepository.GetByDoctor(HttpContext.User.Identity.GetGuidUserId(), page)
                    .ToPagedList(page, 20);
            model.Appointments = list;
            return View(model);
        }

        public ActionResult Approve(Guid id)
        {
            try
            {
                var x = _appointmentRepository.Approve(id);
                if (x)
                {
                    TempData["Message"] = "Success";
                    return RedirectToAction("Index");
                }
                TempData["Message"] = "Error";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["Message"] = "Error";
                return RedirectToAction("Index");
            }
        }




        [HttpGet]
        public ActionResult SetDate(Guid id)
        {
            try
            {
                var models = _appointmentRepository.GetById(id);
                if (models != null)
                {
                    var model = new AppointmentSetDateViewModel()
                    {
                        Date = models.AppointmentDate,
                        Id = models.Id
                    };
                    return View(model);
                }
                TempData["Message"] = "Error";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["Message"] = "Error";
                return RedirectToAction("Index");

            }
        }

        public ActionResult Delete(Guid id)
        {
            try
            {
                _appointmentRepository.Delete(p => p.Id == id);
                TempData["Message"] = "Success";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SetDate(AppointmentSetDateViewModel model)
        {
            try
            {
                var x = _appointmentRepository.Approve(model.Id, model.Date.Add(model.Time.TimeOfDay));
                if (x)
                    return RedirectToAction("Index");
                TempData["Message"] = "Error";
                return RedirectToAction("Index");

            }
            catch (Exception)
            {
                TempData["Message"] = "Error";
                return RedirectToAction("Index");
            }
        }



    }
}