﻿using System;
using System.Web.Mvc;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;
using MVCC.Website.Models;

namespace MVCC.Website.Controllers
{
    public class EventController : Controller
    {
        private readonly EventRepository _eventRepository;
        public EventController(EventRepository repository)
        {
            _eventRepository = repository;
        }
        // GET: Event
        [HttpGet]
        public ActionResult Index()
        {
            var list = _eventRepository.GetAll();
            EventViewModel model = new EventViewModel()
            {
                Events = list
            };
            return View(model);
        }

        [HttpGet]
        [ActionName("Create")]

        public ActionResult Create()
        {
            return View(new EventRegsiterViewModel());
        }



        [HttpPost]
        [ActionName("Create")]

        public ActionResult Create(EventRegsiterViewModel rmodel)
        {
            var model = new Event()
            {
                Name = rmodel.Name,
                DateCreated = DateTime.Now,
                DateUpdated = DateTime.Now,
                DateofEvent = rmodel.DateofEvent,
                Description = rmodel.Description,
                Location = rmodel.Location

            };

            if (_eventRepository.Add(model) != null)
            {
                TempData["Message"] = "Success";
                return RedirectToAction("Index");
            }
            TempData["Message"] = "Error in Input";
            return View(model);
        }

        [HttpGet]
        [ActionName("Update")]
        public ActionResult Update(Guid id)
        {
            var model = _eventRepository.GetById(id);
            if (model != null)
            {
                EventUpdateViewModel Model = new EventUpdateViewModel()
                {
                    Name = model.Name,
                    DateofEvent = model.DateofEvent,
                    Id = model.Id,
                    Description = model.Description,
                    Location = model.Location
                };
                return View(Model);
            }
            TempData["Message"] = "Error Not Found";
            return RedirectToAction("Index");
        }

        [HttpGet]
        [ActionName("Delete")]
        public ActionResult Delete(Guid id)
        {

            var model = _eventRepository.GetById(id);
            if (model != null)
            {
                _eventRepository.Delete(m => m.Id == model.Id);
                TempData["Message"] = "Successfully Deleted";
                return RedirectToAction("Index");
            }
            TempData["Message"] = "Error Not Found";
            return RedirectToAction("Index");
        }


        [HttpPost]
        [ActionName("Update")]
        public ActionResult Update(EventUpdateViewModel model)
        {
            model.DateUpdated = DateTime.Now;
            var Model = new Event()
            {
                Id = model.Id,
                DateUpdated = model.DateUpdated,
                DateofEvent = model.DateofEvent,
                Description = model.Description,
                Name = model.Name,
                Location = model.Location
            };
            if (_eventRepository.Update(Model))
            {
                TempData["Message"] = "Error Updating";
            }
            else
            {
                TempData["Message"] = "Updated";
            }
            return RedirectToAction("Index");
        }


    }
}