﻿
using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;
using MVCC.Service.Managers;
using MVCC.Website.Models;

namespace MVCC.Website.Controllers
{
    public class MessageController : Controller
    {

        private readonly MessageRepository _messageRepository;
        private readonly ApplicationUserManager _userManager;

        public MessageController(MessageRepository repository,ApplicationUserManager manager)
        {
            _messageRepository = repository;
            _userManager = manager;
        }
        // GET: Message
        
        public ActionResult Index(Guid id)
        {
            var model = new MessageIndexViewModel {Id = id.ToString()};
            var name = _userManager.FindById(id)?.FullName;
            if (name == "") return RedirectToAction("Index", "Home");
            var ids = HttpContext.User.Identity.GetUserId();
            model.FullName = name;
            model.Id = id.ToString();
            model.Messages = _messageRepository.GetMessages(id, Guid.Parse(ids));
            return View(model);
        }

        [System.Web.Http.HttpGet]
        public ActionResult Recieve(Guid id)
        {
            var ids = HttpContext.User.Identity.GetUserId();
            var list = _messageRepository.GetMessages(id, Guid.Parse(ids));
            return Json(list,JsonRequestBehavior.AllowGet);
        }
        [System.Web.Mvc.HttpPost]
        public IHttpActionResult Send([FromBody] string toId, [FromBody]string context)
        {
            var message = new Message()
            {
                Context = context,
                DateSent = DateTime.Now,
                DateRecieved = DateTime.Now,
                DateRead = DateTime.Now,
                FromId = Guid.Parse(HttpContext.User.Identity.GetUserId()),
                ToId = Guid.Parse(toId),
            };
            if(_messageRepository.Add(message) != null)
                return new OkResult(new HttpRequestMessage());
            return new BadRequestResult(new HttpRequestMessage());
        }
        
    }
}