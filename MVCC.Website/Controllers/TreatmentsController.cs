﻿// -----------------------------------------------------------------------
// <copyright file="TreatmentsController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Controllers
{
    #region Usings

    using System.Web.Mvc;

    #endregion Usings

    [AllowAnonymous]
    public class TreatmentsController : Controller
    {
        public ActionResult Brachytherapy()
        {
            this.ViewBag.Title = "Brachytherapy";
            return this.View();
        }

        public ActionResult Chemotherapy()
        {
            this.ViewBag.Title = "Chemotherapy";
            return this.View();
        }

        public ActionResult Cyberknife()
        {
            this.ViewBag.Title = "Cyberknife";
            return this.View();
        }

        // GET: Treatments
        public ActionResult Index()
        {
            this.ViewBag.Title = "Treatments";
            return this.View();
        }

        public ActionResult NuclearMedicine()
        {
            this.ViewBag.Title = "Nuclear Medicine";
            return this.View();
        }

        public ActionResult Radiotherapy()
        {
            this.ViewBag.Title = "Radiotherapy";
            return this.View();
        }

        public ActionResult Research()
        {
            this.ViewBag.Title = "Research";
            return this.View();
        }
    }
}