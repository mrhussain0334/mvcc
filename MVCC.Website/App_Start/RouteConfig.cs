﻿// -----------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website
{
    #region Usings

    using System.Web.Mvc;
    using System.Web.Routing;

    #endregion Usings

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*path}", new { path = @"API\/(.*)" });
            //routes.MapRoute("WebApi", "api/{controller}/{action}/{id}");
            routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}