﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using MVCC.Model.Entities;

namespace MVCC.Website.Hub
{

    [HubName("chatHub")]
    public class ChatHub : Microsoft.AspNet.SignalR.Hub
    {

        public void Send(string name, string message)
        {
            Clients.All.addNewMessageToPage(name, message);
        }
        public Task Subscribe(string id)
        {
            return Groups.Add(Context.ConnectionId, id);
        }

        public Task UnSubscribe(string id)
        {
            return Groups.Remove(Context.ConnectionId, id);
        }
    }
}