﻿$(function () {
    $(".inputbox")
        .keypress(function (e) {
            if (e.keyCode === 13) {
                if ($(this).val() !== "") {
                    sendMessage(getUrlParameter("id"), $(this).val());
                }
            }
        });
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
function sendMessage(id, message) {
    $.post(
        "/MVCC/Message/Send",
        {
            toId: id,
            context: message
        },
        function (data, status) {
            if (status === "success") {
                $("#containermessage")
                    .append('<div class="panel panel-info">' +
                        '<div class="panel-heading">' +
                        '<div style="float: right">' +
                        getISODateTime(new Date($.now())) +
                        '</div>' +
                        '<div style="float: left">' +
                        '<bold>Me</bold> : ' +
                        '</div>' +
                        message +
                        '</div>' +
                        '</div>');
            }
        }
        );
}


function getISODateTime(d) {
    // padding function
    var s = function (a, b) { return (1e15 + a + "").slice(-b) };

    // default date parameter
    if (typeof d === 'undefined') {
        d = new Date();
    };

    // return ISO datetime
    return d.getFullYear() + '-' +
        s(d.getMonth() + 1, 2) + '-' +
        s(d.getDate(), 2) + ' ' +
        s(d.getHours(), 2) + ':' +
        s(d.getMinutes(), 2) + ':' +
        s(d.getSeconds(), 2);
}
function recievedMessage() {
    $.get(
        "/MVCC/Message/Recieve?id=" + getUrlParameter("id"),
        function(data, status) {
            if (status === "success") {
                var list = $.parseJSON(($.toJSON(data)));
                $("#containermessage").empty();
                for (var i = 0; i < list.length; i++) {
                    $("#containermessage")
                        .append('<div class="panel panel-info">' +
                            '<div class="panel-heading">' +
                            '<div style="float: right">' +
                            list[i].DateSend +
                            '</div>' +
                            '<div style="float: left">' +
                            list[i].From.FullName +
                            '     </div>' +
                            list[i].Context +
                            ' </div>   ' +
                            '</div>');
                }
            }
        });
}