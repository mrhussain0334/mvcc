﻿// -----------------------------------------------------------------------
// <copyright file="Startup.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

#region Usings

using Microsoft.Owin;
using MVCC.Website;

#endregion Usings

[assembly: OwinStartup("Website", typeof(Startup))]

namespace MVCC.Website
{
    #region Usings

    using Owin;

    #endregion Usings

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}