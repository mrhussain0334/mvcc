﻿// -----------------------------------------------------------------------
// <copyright file="AdminViewModel.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Models
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    #endregion Usings

    public class EditUserViewModel
    {
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        public Guid Id { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }
    }

    public class RoleViewModel
    {
        public IEnumerable<SelectListItem> ClaimsList { get; set; }

        public Guid Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "RoleName")]
        public string Name { get; set; }
    }
}