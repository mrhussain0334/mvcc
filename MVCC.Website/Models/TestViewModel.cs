﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MVCC.Model.Entities;

namespace MVCC.Website.Models
{
    public class TestIndexViewModel
    {
        public IEnumerable<Test> Tests { get; set; }
    }


    public class TestCreateViewModel
    {
        [Required]
        public Guid DoctorId { get; set; }
        [Required]
        public Guid PatientId { get; set; }
        public IEnumerable<ApplicationUser> Patients { get; set; }

        [Required]
        [DisplayName("Test Date")]
        public DateTime TestDate { get; set; }
        //Orange = 0 || Green = 1
        [Required]
        [DisplayName("Color")]
        public int Color { get; set; }
        [Required]
        [DisplayName("Message To Patient")]
        public string Message { get; set; }
    }
    public class TestRegisterViewModel
    {
        [Required]
        public Guid DoctorId { get; set; }
        [Required]
        public Guid PatientId { get; set; }
        [Required]
        [DisplayName("Test Date")]
        public DateTime TestDate { get; set; }
        //Orange = 0 || Green = 1
        [Required]
        [DisplayName("Color")]
        public int Color { get; set; }
        [Required]
        [DisplayName("Message To Patient")]
        public string Message { get; set; }
    }


}