﻿using System.Collections.Generic;
using MVCC.Model.Entities;

namespace MVCC.Website.Models
{
    public class AssignIndexViewModel
    {
        public IEnumerable<ApplicationUser> PatientsList { get; set; }

        public AssignIndexViewModel()
        {
            PatientsList = new List<ApplicationUser>();
        }
    }

    public class AssignPatientViewModel
    {
        public ApplicationUser Patient { get; set; }
        public IEnumerable<ApplicationUser> DoctorsList { get; set; }
    }
}