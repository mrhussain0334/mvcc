﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCC.Model.Entities;

namespace MVCC.Website.Models
{
    public class AppointmentViewModel
    {
        public IEnumerable<Appointment> Appointments { get; set; }
    }

    public class AppointmentSetDateViewModel
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
    }

}