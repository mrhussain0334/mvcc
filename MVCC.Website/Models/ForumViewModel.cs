﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using MVCC.Model.Entities;

namespace MVCC.Website.Models
{
    public class ForumIndexViewModel
    {
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public IEnumerable<Model> Models { get; set; }
        public class  Model
        {
            public Category Category { get; set; }
            public int Count { get; set; }
        }
    }

    public class ForumCategoryViewModel
    {
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public IEnumerable<Discussion> Discussions { get; set; }
    }

    public class ForumCreateViewModel
    {
        [Required]
        [DisplayName("Enter Topic Name")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }

        [Required]
        [DisplayName("Select Category")]
        public Guid CategoryId { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }

    public class DiscussionViewModel
    {
        public Guid Id { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreaterName { get; set; }
        public string Category { get; set; }
        public IEnumerable<Comment> Comments { get; set; }

        public CommentCreateViewModel CommentCreateViewModel { get; set; }
    }

    public class DiscussionEditViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        [DisplayName("Enter Topic Name")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }

        [Required]
        [DisplayName("Select Category")]
        public Guid CategoryId { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
    public class CommentCreateViewModel
    {
        [Required]
        public Guid DiscussionId { get; set; }
        public Guid CommenterId { get; set; }
        [Required]
        public string Context { get; set; }
        public DateTime DateCommented { get; set; }

    }
}