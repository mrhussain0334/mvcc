﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCC.Model.Entities;

namespace MVCC.Website.Models
{
    public class ProfileDiscussionViewModel
    {
        public IEnumerable<Discussion> Discussions { get; set; }
        public int PageCount { get; set; }
        public int PageNumber { get; set; }

    }
}