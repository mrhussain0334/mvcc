﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MVCC.Model.Entities;

namespace MVCC.Website.Models
{

    public class MessageIndexViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public IEnumerable<Message> Messages { get; set; }

        public MessageIndexViewModel()
        {
            
        }
    }

    public class MesssageViewModel
    {
        public MesssageViewModel()
        {

        }

        public MesssageViewModel(Message message)
        {
            Context = message.Context;
            Sender = message.From.FullName;
            Reciever = message.To.FullName;
        }


        [Required]
        public Guid SendId { get; set; }

        [Required]
        public Guid RecieverId { get; set; }

        [Required]
        public string Sender { get; set; }

        [Required]
        public string Reciever { get; set; }

        [Required]
        public string Context { get; set; }
    }

    public class MessageFullViewModel
    {
    }
}