﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MVCC.Model.Entities;

namespace MVCC.Website.Models
{
    public class CategoryIndexViewModel
    {
        public IEnumerable<Category> Categories { get; set; }
    }

    public class CategoryCreateViewModel
    {
        public string CategoryName { get; set; }
    }

    public class CategoryUpdateViewModel
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public Category Category { get; set; }
    }
}