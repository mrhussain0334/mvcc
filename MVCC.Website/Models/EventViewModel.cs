﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MVCC.Model.Entities;

namespace MVCC.Website.Models
{
    public class EventViewModel
    {
       public IEnumerable<Event> Events { get; set; }
    }
    public class EventRegsiterViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Event Name")]
        public string Name { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 15)]
        [DataType(DataType.Text)]
        [Display(Name = "Event Description")]
        public string Description { get; set; }


        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Address of Event")]
        public string Location { get; set; }



        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Event")]
        public DateTime DateofEvent { get; set; }

        
        [DataType(DataType.Date)]
        public DateTime DateCreated;

        public EventRegsiterViewModel()
        {
            DateCreated = DateTime.Today;
        }
    }

    public class EventUpdateViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Event Name")]
        public string Name { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 15)]
        [Display(Name = "Event Description")]
        public string Description { get; set; }


        [Required]
        [Display(Name = "Location/Address of Event")]
        public string Location { get; set; }



        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Event of Event")]
        public DateTime DateofEvent { get; set; }


        [DataType(DataType.Date)]
        public DateTime DateUpdated;

    }
}