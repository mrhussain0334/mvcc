﻿// -----------------------------------------------------------------------
// <copyright file="MvcExtensions.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Extensions
{
    #region Usings

    using System.Web.Mvc;
    using System.Web.Routing;

    #endregion Usings

    public static class MvcExtensions
    {
        public static string IsActive(this HtmlHelper html, string control, string action)
        {
            RouteData routeData = html.ViewContext.RouteData;

            string routeAction = (string)routeData.Values["action"];
            string routeControl = (string)routeData.Values["controller"];

            bool returnActive = control == routeControl && action == routeAction;

            return returnActive ? "active" : "";
        }

        public static string IsControllerActive(this HtmlHelper html, params string[] controllers)
        {
            RouteData routeData = html.ViewContext.RouteData;

            string routeControl = (string)routeData.Values["controller"];

            foreach (string controller in controllers)
            {
                if (controller == routeControl)
                {
                    return "active";
                }
            }

            return "";
        }
    }
}