﻿// -----------------------------------------------------------------------
//  <copyright file="RoleClaimAuthorizeAttribute.cs" company="Mount Vernon Cancer Centre">
//      Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
//  </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Attributes
{
    #region Usings

    using System.Web;
    using System.Web.Mvc;
    using Model.Entities;
    using Web.Shared.Extensions;

    #endregion

    public class RoleClaimAuthorizeAttribute : AuthorizeAttribute
    {
        public RoleClaim RoleClaim { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.User.HasRoleClaim(this.RoleClaim);
        }
    }
}