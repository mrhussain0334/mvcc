﻿// -----------------------------------------------------------------------
// <copyright file="SecurityExtensions.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Web.Shared.Extensions
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using Model.Entities;

    #endregion Usings

    public static class SecurityExtensions
    {
        public static bool HasRoleClaim(this IPrincipal user, RoleClaim roleClaim)
        {
            foreach (RoleClaim claims in GetClaims(user.Identity as ClaimsIdentity))
            {
                if ((roleClaim & claims) > 0)
                {
                    return true;
                }
            }

            return false;
        }

        private static IEnumerable<RoleClaim> GetClaims(ClaimsIdentity ident)
        {
            return ident == null
                ? Enumerable.Empty<RoleClaim>()
                : ident.Claims.Where(c => c.Type == "RoleClaims")
                    .Select(c => (RoleClaim)Enum.Parse(typeof(RoleClaim), string.Join(",", c.Value)));
        }
    }
}