﻿// -----------------------------------------------------------------------
// <copyright file="IdentityExtensions.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Web.Shared.Extensions
{
    #region Usings

    using System;
    using System.Security.Principal;
    using Microsoft.AspNet.Identity;

    #endregion Usings

    public static class IdentityExtensions
    {
        public static Guid GetGuidUserId(this IIdentity identity)
        {
            Guid result = Guid.Empty;
            Guid.TryParse(identity.GetUserId(), out result);
            return result;
        }
    }
}