﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationUserStore.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Data.Stores
{
    #region Usings

    using System;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Model.Entities;

    #endregion Usings

    public class ApplicationUserStore :
        UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>,
        IUserStore<ApplicationUser, Guid>
    {
        public ApplicationUserStore(ApplicationContext context) : base(context)
        {
        }
    }
}