﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationRoleStore.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Data.Stores
{
    #region Usings

    using System;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Model.Entities;

    #endregion Usings

    public class ApplicationRoleStore : RoleStore<ApplicationRole, Guid, ApplicationUserRole>
    {
        public ApplicationRoleStore(ApplicationContext context) : base(context)
        {
        }
    }
}