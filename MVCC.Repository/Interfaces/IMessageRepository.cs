﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCC.Model.Entities;

namespace MVCC.Data.Interfaces
{
    public interface IMessageRepository
    {
        IEnumerable<Message> GetMessages(Guid from,Guid? to,int limit);
    }
}
