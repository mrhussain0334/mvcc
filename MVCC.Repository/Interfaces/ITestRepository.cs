﻿// -----------------------------------------------------------------------
// <copyright file="ITestRepository.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace MVCC.Data.Interfaces
{
    #region Usings

    using Infrastructure;
    using Model.Entities;

    #endregion Usings

    public interface ITestRepository
    {
        IEnumerable<Test> GetTest(Guid? doctor, int limit);
    }
}