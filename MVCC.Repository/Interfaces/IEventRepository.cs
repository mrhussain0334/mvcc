﻿using System.Collections.Generic;
using MVCC.Model.Entities;

namespace MVCC.Data.Interfaces
{
    public interface IEventRepository 
    {
        IEnumerable<Event> GetEventsByMonth(int Month);
        IEnumerable<Event> GetEventsByYear(int Month);

    }
}
