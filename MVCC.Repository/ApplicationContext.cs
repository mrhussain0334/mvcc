﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationContext.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Data
{
    #region Usings

    using System;
    using System.Data.Entity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Model.Entities;

    #endregion Usings

    public class ApplicationContext :
        IdentityDbContext<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationContext() : base("Name=DefaultConnection")
        {
                
        }


        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Discussion> Discussions { get; set; }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Message> Messages { get; set; }
        
        public virtual void Commit()
        {
            this.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

//        public IDbSet<ApplicationUser> ApplicationUsers { get; set; }
    }
}