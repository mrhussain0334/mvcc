﻿using System;
using System.Collections.Generic;
using System.Linq;
using MVCC.Data.Infrastructure;
using MVCC.Data.Interfaces;
using MVCC.Model.Entities;

namespace MVCC.Data.Repositories
{
    public class EventRepository : RepositoryBase<Event> , IEventRepository
    {
        public EventRepository(ApplicationContext context) : base(context)
        {

        }

        public IEnumerable<Event> GetEventsByMonth(int Month)
        {
            return EventContext.Events.Where(p => p.DateofEvent.Month == Month).ToList();
        }

        public IEnumerable<Event> GetEventsByYear(int Year)
        {
            return EventContext.Events.Where(p => p.DateofEvent.Year == Year).ToList();
        }

        public new bool Update(Event e)
        {
            var obj = EventContext.Events.Find(e.Id);
            if (obj == null) return false;
            obj.Name = e.Name;
            obj.DateofEvent = e.DateofEvent;
            obj.DateUpdated = DateTime.Today;
            obj.Description = e.Description;
            obj.Location = e.Location;
            var x =dbContext.SaveChanges();
            return x != 0;
        }

        public ApplicationContext EventContext => dbContext;
    }
}
