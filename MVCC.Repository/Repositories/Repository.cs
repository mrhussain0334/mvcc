﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MVCC.Data.Infrastructure;
using MVCC.Model.Entities;

namespace MVCC.Data.Repositories
{
    class Repository<T> : IRepository<T>
        where T : EntityBase
    {
        protected DbContext Context;


        public Repository(DbContext context)
        {
            Context = context;
        }

        public T Add(T entity)
        {
            try
            {
                return Context.Set<T>().Add(entity);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public void Delete(Expression<Func<T, bool>> where)
        {
            try
            {
                T entity = Context.Set<T>().Find(where);
                if (entity != null)
                {
                    Context.Set<T>().Remove(entity);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }

        public T Get(Expression<Func<T, bool>> where)
        {
            try
            {
                return Context.Set<T>().Find(where);
                
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public IEnumerable<T> GetAll()
        {
            return Context.Set<T>().ToList();
        }

        public T GetById(Guid id)
        {
            return Context.Set<T>().Find(id);
        }

        public IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return Context.Set<T>().Where(where).ToList();
        }

        public void Update(T entity)
        {
            T obj = Context.Set<T>().Find(entity.Id);
            if (obj != null)
            {
                
            }
        }
    }
}
