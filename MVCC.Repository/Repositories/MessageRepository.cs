﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCC.Data.Infrastructure;
using MVCC.Data.Interfaces;
using MVCC.Model.Entities;

namespace MVCC.Data.Repositories
{
    public class MessageRepository : RepositoryBase<Message>, IMessageRepository
    {
        public MessageRepository(ApplicationContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Message> GetMessages(Guid @from, Guid? to, int limit = 50)
        {
            return
                MessageContext.Where(m => (m.FromId == from || m.FromId == to) && (m.ToId == from || m.ToId == to))
                    .OrderByDescending(m => m.DateSent).Take(limit).ToList();

        }


        public bool Read(Message entity)
        {
            var obj = MessageContext.Find(entity.Id);
            if (obj == null) return false;
            obj.IsRead = true;
            obj.DateRead = DateTime.Now;
            var x = dbContext.SaveChanges();
            return x != 0;
        }

        public bool Recieved(Message entity)
        {
            var obj = MessageContext.Find(entity.Id);
            if (obj == null) return false;
            obj.IsRecieved = true;
            obj.DateRecieved = DateTime.Now;
            var x = dbContext.SaveChanges();
            return x != 0;
        }

        

        public DbSet<Message> MessageContext => dbContext.Messages;

    }
}
