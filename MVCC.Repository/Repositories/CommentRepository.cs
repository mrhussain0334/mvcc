﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MVCC.Data.Infrastructure;
using MVCC.Model.Entities;
using PagedList;

namespace MVCC.Data.Repositories
{
    public class CommentRepository : RepositoryBase<Comment>
    {
        public CommentRepository(ApplicationContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Comment> GetByDiscussion(Guid id)
        {
            return Comments.Where(c => c.DiscussionId == id).ToList();
        }

        public IEnumerable<Comment> GetByDiscussion(Guid id,int page,int count)
        {
            return Comments.Where(c => c.DiscussionId == id).OrderBy(c => c.CommentedDate).ToPagedList(page,count);
        }

        public int GetCountByDiscussion(Guid id,int size)
        {
            return (Comments.Count(p => p.DiscussionId == id) / size) + 1;
        }

        private DbSet<Comment> Comments  => dbContext.Comments;

    }
}
