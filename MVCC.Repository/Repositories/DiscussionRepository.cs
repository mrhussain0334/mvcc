﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MVCC.Data.Infrastructure;
using MVCC.Model.Entities;

namespace MVCC.Data.Repositories
{
    public class DiscussionRepository : RepositoryBase<Discussion>
    {
        public DiscussionRepository(ApplicationContext dbContext) : base(dbContext)
        {

        }

        public int Pages(int perPage)
        {
            return (Discussions.Count()/perPage) + 1;
        }

        public int CountByCategory(Category category)
        {
            return Discussions.Count(d => d.Category.Id == category.Id);
        }

        public IEnumerable<Discussion> GetByCategory(Category category)
        {
            return Discussions.Where(d => d.CategoryId == category.Id).ToList();
        }

        public IEnumerable<Discussion> GetByCategory(Guid id)
        {
            return Discussions.Where(d => d.CategoryId == id).ToList();
        }

        public IEnumerable<Discussion> GetByUser(Guid id)
        {
            return Discussions.Where(d => d.CreaterId == id).ToList();
        }


        public bool Delete(Guid id)
        {
            try
            {
                var model = GetById(id);
                if (model != null)
                {
                    Delete(model);
                }

                return true;
            }
            catch (Exception ex)
            {
                
            }
            return false;
        }
        public new bool Update(Discussion discussion)
        {
            try
            {
                var model = GetById(discussion.Id);
                if (model != null)
                {
                    model.CategoryId = discussion.CategoryId;
                    model.DateUpdated = DateTime.Now;
                    model.Name = discussion.Name;
                    model.Description = discussion.Description;
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                
            }
            return false;
        }
        private DbSet<Discussion> Discussions => dbContext.Discussions;
    }
}
