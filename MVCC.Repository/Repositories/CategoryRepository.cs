﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCC.Data.Infrastructure;
using MVCC.Model.Entities;

namespace MVCC.Data.Repositories
{
    public class CategoryRepository : RepositoryBase<Category>
    {
        public CategoryRepository(ApplicationContext dbContext) : base(dbContext)
        {
        }

        public new bool Update(Category category)
        {
            var cat = Categories.Find(category.Id);
            if (cat == null) return false;
            cat.DateUpdated = DateTime.Now;
            cat.Name = category.Name;
            return dbContext.SaveChanges() != 0;
        }

        private DbSet<Category> Categories => dbContext.Categories;
    }
}
