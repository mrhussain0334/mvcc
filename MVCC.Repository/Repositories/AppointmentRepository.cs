﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCC.Data.Infrastructure;
using MVCC.Model.Entities;
using PagedList;

namespace MVCC.Data.Repositories
{
    public class AppointmentRepository : RepositoryBase<Appointment>
    {
        public AppointmentRepository(ApplicationContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<Appointment> GetByPatient(Guid id, int page)
        {
            return
                Appointments.Where(p => p.PatientId == id && p.IsApproved)
                    .OrderByDescending(p => p.AppointmentDate)
                    .ToPagedList(page, 20);
        }

        
        
        public IEnumerable<Appointment> GetByDoctor(Guid id, int page)
        {
            return Appointments.Where(p => p.DoctorId == id).OrderByDescending(p => p.AppointmentDate).ToPagedList(page, 20);
        }
        public bool Approve(Guid id, DateTime dateTime)
        {
            var appointment = Appointments.FirstOrDefault(p => p.Id == id);
            if (appointment == null) return false;
            appointment.IsApproved = true;
            appointment.AppointmentDate = dateTime;
            appointment.AcceptedDate = dateTime;
            return dbContext.SaveChanges() != -1;
        }
        public bool Approve(Guid id)
        {
            var appointment = Appointments.FirstOrDefault(p => p.Id == id);
            if (appointment == null) return false;
            appointment.IsApproved = true;
            return dbContext.SaveChanges() != -1;
        }


        private DbSet<Appointment> Appointments => dbContext.Appointments;
    }
}
