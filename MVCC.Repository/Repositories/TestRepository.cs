﻿// -----------------------------------------------------------------------
// <copyright file="TestRepository.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MVCC.Data.Repositories
{
    #region Usings

    using Infrastructure;
    using Interfaces;
    using Model.Entities;

    #endregion Usings

    public class TestRepository : RepositoryBase<Test>, ITestRepository
    {

        public TestRepository(ApplicationContext dbContext) : base(dbContext)
        {

        }

        private DbSet<Test> TestContext => dbContext.Tests;

        public IEnumerable<Test> GetTest(Guid? doctor, int limit = 50)
        {
            return TestContext.Where(t => t.DoctorId == doctor).OrderByDescending(t => t.TestDate).Take(limit).ToList();
        }

        public IEnumerable<Test> GetTestByPatient(Guid id)
        {
            return TestContext.Where(t => t.PatientId == id).ToList();
        }
    }
}