namespace MVCC.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddtoDoctor : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Messages", "From_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "To_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Messages", new[] { "From_Id" });
            DropIndex("dbo.Messages", new[] { "To_Id" });
            DropColumn("dbo.Messages", "FromId");
            DropColumn("dbo.Messages", "ToId");
            RenameColumn(table: "dbo.Messages", name: "From_Id", newName: "FromId");
            RenameColumn(table: "dbo.Messages", name: "To_Id", newName: "ToId");
            AddColumn("dbo.Categories", "DateUpdated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Discussions", "Description", c => c.String());
            AddColumn("dbo.Messages", "IsRecieved", c => c.Boolean(nullable: false));
            AddColumn("dbo.Tests", "DoctorId", c => c.Guid());
            AddColumn("dbo.Tests", "PatientId", c => c.Guid());
            AddColumn("dbo.Tests", "TestDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Tests", "Color", c => c.Int(nullable: false));
            AlterColumn("dbo.Messages", "FromId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Messages", "ToId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Messages", "FromId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Messages", "ToId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Messages", "FromId");
            CreateIndex("dbo.Messages", "ToId");
            CreateIndex("dbo.Tests", "DoctorId");
            CreateIndex("dbo.Tests", "PatientId");
            AddForeignKey("dbo.Tests", "DoctorId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Tests", "PatientId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Messages", "FromId", "dbo.AspNetUsers", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Messages", "ToId", "dbo.AspNetUsers", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "ToId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "FromId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Tests", "PatientId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Tests", "DoctorId", "dbo.AspNetUsers");
            DropIndex("dbo.Tests", new[] { "PatientId" });
            DropIndex("dbo.Tests", new[] { "DoctorId" });
            DropIndex("dbo.Messages", new[] { "ToId" });
            DropIndex("dbo.Messages", new[] { "FromId" });
            AlterColumn("dbo.Messages", "ToId", c => c.Guid());
            AlterColumn("dbo.Messages", "FromId", c => c.Guid());
            AlterColumn("dbo.Messages", "ToId", c => c.Int(nullable: false));
            AlterColumn("dbo.Messages", "FromId", c => c.Int(nullable: false));
            DropColumn("dbo.Tests", "Color");
            DropColumn("dbo.Tests", "TestDate");
            DropColumn("dbo.Tests", "PatientId");
            DropColumn("dbo.Tests", "DoctorId");
            DropColumn("dbo.Messages", "IsRecieved");
            DropColumn("dbo.Discussions", "Description");
            DropColumn("dbo.Categories", "DateUpdated");
            RenameColumn(table: "dbo.Messages", name: "ToId", newName: "To_Id");
            RenameColumn(table: "dbo.Messages", name: "FromId", newName: "From_Id");
            AddColumn("dbo.Messages", "ToId", c => c.Int(nullable: false));
            AddColumn("dbo.Messages", "FromId", c => c.Int(nullable: false));
            CreateIndex("dbo.Messages", "To_Id");
            CreateIndex("dbo.Messages", "From_Id");
            AddForeignKey("dbo.Messages", "To_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Messages", "From_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
