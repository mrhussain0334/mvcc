namespace MVCC.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddtoDiscussion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "CreaterId", c => c.Guid(nullable: false));
            AddColumn("dbo.Discussions", "CreaterId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Categories", "CreaterId");
            CreateIndex("dbo.Discussions", "CreaterId");
            AddForeignKey("dbo.Categories", "CreaterId", "dbo.AspNetUsers", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Discussions", "CreaterId", "dbo.AspNetUsers", "Id", cascadeDelete: false);
            
        }

        public override void Down()
        {
            DropForeignKey("dbo.Discussions", "CreaterId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Categories", "CreaterId", "dbo.AspNetUsers");
            DropIndex("dbo.Discussions", new[] { "CreaterId" });
            DropIndex("dbo.Categories", new[] { "CreaterId" });
            DropColumn("dbo.Discussions", "CreaterId");
            DropColumn("dbo.Categories", "CreaterId");
            
        }
    }
}
