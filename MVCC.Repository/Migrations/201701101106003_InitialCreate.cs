// -----------------------------------------------------------------------
// <copyright file="201701101106003_InitialCreate.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Data.Migrations
{
    #region Usings

    using System.Data.Entity.Migrations;

    #endregion Usings

    public partial class InitialCreate : DbMigration
    {
        public override void Down()
        {
            this.DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            this.DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            this.DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            this.DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            this.DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            this.DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            this.DropIndex("dbo.AspNetUsers", "UserNameIndex");
            this.DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            this.DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            this.DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            this.DropTable("dbo.AspNetUserLogins");
            this.DropTable("dbo.AspNetUserClaims");
            this.DropTable("dbo.AspNetUsers");
            this.DropTable("dbo.Tests");
            this.DropTable("dbo.AspNetUserRoles");
            this.DropTable("dbo.AspNetRoles");
        }

        public override void Up()
        {
            this.CreateTable("dbo.AspNetRoles", c => new { Id = c.Guid(false), Claims = c.Int(false), Name = c.String(false, 256) })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");

            this.CreateTable("dbo.AspNetUserRoles", c => new { UserId = c.Guid(false), RoleId = c.Guid(false) })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            this.CreateTable("dbo.Tests", c => new { Id = c.Guid(false, true), Message = c.String() }).PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.AspNetUsers",
                c =>
                    new
                    {
                        Id = c.Guid(false, true),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(false),
                        TwoFactorEnabled = c.Boolean(false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(false),
                        AccessFailedCount = c.Int(false),
                        UserName = c.String(false, 256)
                    }).PrimaryKey(t => t.Id).Index(t => t.UserName, unique: true, name: "UserNameIndex");

            this.CreateTable(
                    "dbo.AspNetUserClaims",
                    c => new { Id = c.Int(false, true), UserId = c.Guid(false), ClaimType = c.String(), ClaimValue = c.String() })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, true)
                .Index(t => t.UserId);

            this.CreateTable(
                    "dbo.AspNetUserLogins",
                    c => new { LoginProvider = c.String(false, 128), ProviderKey = c.String(false, 128), UserId = c.Guid(false) })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, true)
                .Index(t => t.UserId);
        }
    }
}