namespace MVCC.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddtoComment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Context = c.String(),
                        CommenterId = c.Guid(nullable: false),
                        DiscussionId = c.Guid(nullable: false),
                        CommentedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CommenterId, cascadeDelete: true)
                .ForeignKey("dbo.Discussions", t => t.DiscussionId, cascadeDelete: true)
                .Index(t => t.CommenterId)
                .Index(t => t.DiscussionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "DiscussionId", "dbo.Discussions");
            DropForeignKey("dbo.Comments", "CommenterId", "dbo.AspNetUsers");
            DropIndex("dbo.Comments", new[] { "DiscussionId" });
            DropIndex("dbo.Comments", new[] { "CommenterId" });
            DropTable("dbo.Comments");
        }
    }
}
