// <auto-generated />
namespace MVCC.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddtoAppointment : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddtoAppointment));
        
        string IMigrationMetadata.Id
        {
            get { return "201703120855283_AddtoAppointment"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
