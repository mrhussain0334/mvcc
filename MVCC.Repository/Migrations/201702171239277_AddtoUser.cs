namespace MVCC.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddtoUser : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Discussions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        CategoryId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Location = c.String(),
                        DateofEvent = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Context = c.String(),
                        FromId = c.Int(nullable: false),
                        ToId = c.Int(nullable: false),
                        DateSent = c.DateTime(nullable: false),
                        DateRecieved = c.DateTime(nullable: false),
                        DateRead = c.DateTime(nullable: false),
                        IsRead = c.Boolean(nullable: false),
                        From_Id = c.Guid(),
                        To_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.From_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.To_Id)
                .Index(t => t.From_Id)
                .Index(t => t.To_Id);
            
            AddColumn("dbo.AspNetUsers", "Name", c => c.String());
            AddColumn("dbo.AspNetUsers", "IsApproved", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "AssignTo", c => c.Guid());
            CreateIndex("dbo.AspNetUsers", "AssignTo");
            AddForeignKey("dbo.AspNetUsers", "AssignTo", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "To_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "From_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "AssignTo", "dbo.AspNetUsers");
            DropForeignKey("dbo.Discussions", "CategoryId", "dbo.Categories");
            DropIndex("dbo.AspNetUsers", new[] { "AssignTo" });
            DropIndex("dbo.Messages", new[] { "To_Id" });
            DropIndex("dbo.Messages", new[] { "From_Id" });
            DropIndex("dbo.Discussions", new[] { "CategoryId" });
            DropColumn("dbo.AspNetUsers", "AssignTo");
            DropColumn("dbo.AspNetUsers", "IsApproved");
            DropColumn("dbo.AspNetUsers", "Name");
            DropTable("dbo.Messages");
            DropTable("dbo.Events");
            DropTable("dbo.Discussions");
            DropTable("dbo.Categories");
        }
    }
}
