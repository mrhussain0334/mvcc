namespace MVCC.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddtoAppointment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        AppointmentDate = c.DateTime(nullable: false),
                        AcceptedDate = c.DateTime(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        DoctorId = c.Guid(nullable: false),
                        PatientId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DoctorId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.PatientId, cascadeDelete: false)
                .Index(t => t.DoctorId)
                .Index(t => t.PatientId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Appointments", "PatientId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Appointments", "DoctorId", "dbo.AspNetUsers");
            DropIndex("dbo.Appointments", new[] { "PatientId" });
            DropIndex("dbo.Appointments", new[] { "DoctorId" });
            DropTable("dbo.Appointments");
        }
    }
}
