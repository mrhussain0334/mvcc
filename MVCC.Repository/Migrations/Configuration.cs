// -----------------------------------------------------------------------
// <copyright file="Configuration.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Data.Migrations
{
    #region Usings

    using System;
    using System.Data.Entity.Migrations;
    using Microsoft.AspNet.Identity;
    using Model.Entities;

    #endregion Usings

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
            this.ContextKey = "MVCC.Data.ApplicationContext";
        }

        protected override void Seed(ApplicationContext context)
        {
            // This method will be called after migrating to the latest version.

            // You can use the DbSet<T>.AddOrUpdate() helper extension method to avoid creating
            // duplicate seed data. E.g.
            //
            // context.People.AddOrUpdate( p => p.FullName, new Person { FullName = "Andrew Peters"
            // }, new Person { FullName = "Brice Lambson" }, new Person { FullName = "Rowan Miller" } );
            const string UserName = "CMLee.1601@gmail.com";
            const string Password = "P4ssw0rd!";
            const string PatientRoleName = "Patient";
            const string AdminRoleName = "Admin";
            const string DoctorRoleName = "Doctor";
            PasswordHasher passwordHasher = new PasswordHasher();
            string passwordHash = passwordHasher.HashPassword(Password);

            ApplicationRole patientRole = new ApplicationRole
            {
                Name = PatientRoleName,
                Claims =
                    RoleClaim.CanCreateTests | RoleClaim.CanReadTests | RoleClaim.CanUpdateTests |
                    RoleClaim.CanDeleteTests
            };
            context.Roles.AddOrUpdate(r => r.Name, patientRole);


            ApplicationRole doctorRole = new ApplicationRole
            {
                Name = DoctorRoleName,
                Claims =
                    RoleClaim.CanCreateTests | RoleClaim.CanReadTests | RoleClaim.CanUpdateTests |
                    RoleClaim.CanDeleteTests | RoleClaim.CanMessageUser
            };
            context.Roles.AddOrUpdate(r => r.Name, doctorRole);

            ApplicationRole adminRole = new ApplicationRole
            {
                Name = AdminRoleName,
                Claims =
                    RoleClaim.CanCreateUsers | RoleClaim.CanReadUsers | RoleClaim.CanUpdateUsers |
                    RoleClaim.CanDeleteUsers | RoleClaim.CanCreateRoles | RoleClaim.CanReadRoles |
                    RoleClaim.CanUpdateRoles | RoleClaim.CanDeleteRoles | RoleClaim.CanApproveUser | 
                    RoleClaim.CanCreateCategory | RoleClaim.CanCreateEvents | RoleClaim.CanDeleteCategory | RoleClaim.CanDeleteEvents | RoleClaim.CanUpdateEvents
                    
            };
            context.Roles.AddOrUpdate(r => r.Name, adminRole);


            ApplicationUser user = new ApplicationUser
            {
                Name = "Admin",
                Email = UserName,
                UserName = UserName,
                PasswordHash = passwordHash,
                IsApproved = true,
                SecurityStamp = Guid.NewGuid().ToString()

            };
            context.Users.AddOrUpdate(u => u.UserName, user);

            ApplicationUserRole userPatientRole = new ApplicationUserRole { RoleId = patientRole.Id, UserId = user.Id };
            user.Roles.Add(userPatientRole);

            ApplicationUserRole userAdminRole = new ApplicationUserRole { RoleId = adminRole.Id, UserId = user.Id };
            user.Roles.Add(userAdminRole);

            ApplicationUserRole userDoctorRole = new ApplicationUserRole { RoleId = doctorRole.Id, UserId = user.Id };
            user.Roles.Add(userDoctorRole);

            SampleData(context,doctorRole.Id,patientRole.Id);


        }

        
        public void SampleData(ApplicationContext context,Guid doctorId,Guid patientId)
        {
            var Password = "!QWEasdzxc123";
            PasswordHasher passwordHasher = new PasswordHasher();
            string passwordHash = passwordHasher.HashPassword(Password);

            var p1 = new ApplicationUser()
            {
                Name = "p1",
                Email = "p1@gmail.com",
                UserName = "p1@gmail.com",
                PasswordHash = passwordHash,
                IsApproved = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            context.Users.AddOrUpdate(p => p.UserName,p1);
            ApplicationUserRole userPatientRole = new ApplicationUserRole { RoleId = patientId, UserId = p1.Id };
            p1.Roles.Add(userPatientRole);

            var p2 = new ApplicationUser()
            {
                Name = "p2",
                Email = "p2@gmail.com",
                UserName = "p2@gmail.com",
                PasswordHash = passwordHash,
                IsApproved = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            context.Users.AddOrUpdate(p => p.UserName, p2);
            ApplicationUserRole userPatientRole2 = new ApplicationUserRole { RoleId = patientId, UserId = p2.Id };
            p2.Roles.Add(userPatientRole2);

            
            var p3 = new ApplicationUser()
            {
                Name = "p3",
                Email = "p3@gmail.com",
                UserName = "p3@gmail.com",
                PasswordHash = passwordHash,
                IsApproved = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            context.Users.AddOrUpdate(p => p.UserName, p3);
            ApplicationUserRole userPatientRole3 = new ApplicationUserRole { RoleId = patientId, UserId = p3.Id };
            p3.Roles.Add(userPatientRole3);




            var d1 = new ApplicationUser()
            {
                Name = "d1",
                Email = "d1@gmail.com",
                UserName = "d1@gmail.com",
                PasswordHash = passwordHash,
                IsApproved = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            context.Users.AddOrUpdate(p => p.UserName, d1);
            ApplicationUserRole userPatientRole4 = new ApplicationUserRole { RoleId = doctorId, UserId = d1.Id };
            d1.Roles.Add(userPatientRole4);

            var d2 = new ApplicationUser()
            {
                Name = "d2",
                Email = "d2@gmail.com",
                UserName = "d2@gmail.com",
                PasswordHash = passwordHash,
                IsApproved = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            context.Users.AddOrUpdate(p => p.UserName, d2);
            ApplicationUserRole userPatientRole5 = new ApplicationUserRole { RoleId = doctorId, UserId = d2.Id };
            d2.Roles.Add(userPatientRole5);





        }

    }
}