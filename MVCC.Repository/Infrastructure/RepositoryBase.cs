﻿// -----------------------------------------------------------------------
// <copyright file="RepositoryBase.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Data.Infrastructure
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using Model.Entities;

    #endregion Usings

    public abstract class RepositoryBase<T> where T : EntityBase
    {
        protected DbContext context;
        protected readonly ApplicationContext dbContext;

        private readonly IDbSet<T> dbSet;

        
        protected RepositoryBase(ApplicationContext dbContext)
        {
            this.dbContext = dbContext;
            this.dbSet = this.dbContext.Set<T>();
        }



        #region Implementation

        public virtual T Add(T entity)
        {
            var model = this.dbSet.Add(entity);
            if (model == null) return null;
            this.dbContext.SaveChanges();
            return model;
        }


        public virtual void Delete(T entity)
        {
            this.dbSet.Remove(entity);
            dbContext.SaveChanges();
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            var objects = this.dbSet.Where(where).AsEnumerable();
            foreach (T obj in objects)
            {
                this.dbSet.Remove(obj);
            }
        }

        public T Get(Expression<Func<T, bool>> where)
        {
            return this.dbSet.Where(where).FirstOrDefault();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return this.dbSet.ToList();
        }

        public virtual T GetById(Guid id)
        {
            return this.dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return this.dbSet.Where(where).ToList();
        }

        public virtual void Update(T entity)
        {
            this.dbSet.Attach(entity);
            this.dbContext.Entry(entity).State = EntityState.Modified;
        }

        #endregion Implementation
    }
}