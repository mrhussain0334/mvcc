﻿// -----------------------------------------------------------------------
// <copyright file="IRepository.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Data.Infrastructure
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Model.Entities;

    #endregion Usings

    public interface IRepository<T> where T : EntityBase
    {
        // Marks an entity as new
        T Add(T entity);

        // Marks an entity to be removed
        void Delete(T entity);

        void Delete(Expression<Func<T, bool>> where);

        // Get an entity using delegate
        T Get(Expression<Func<T, bool>> where);

        // Gets all entities of type T
        IEnumerable<T> GetAll();

        // Get an entity by Guid id
        T GetById(Guid id);

        // Gets entities using delegate
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);

        // Marks an entity as modified
        void Update(T entity);
    }
}