﻿// -----------------------------------------------------------------------
// <copyright file="IUnitOfWork.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}