﻿// -----------------------------------------------------------------------
// <copyright file="AccountController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using MVCC.Model.Entities;
using MVCC.Service.Managers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MVCC.API.Controllers
{

    public class AccountController : ApiController
    {

        private readonly ApplicationUserManager _applicationUserManager;
        private readonly ApplicationSignInManager _signInManager;
        public AccountController(ApplicationUserManager applicationUserManager, ApplicationSignInManager signInManager)
        {
            _applicationUserManager = applicationUserManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public async Task<JsonResult<ApplicationUser>> UserGet([FromUri] string id)
        {
            var user =
                await _applicationUserManager.FindByIdAsync(Guid.Parse(id));
            return Json(user);
        }



        [HttpPost]
        public async Task<string> RegisterUser([FromBody]JToken jsonbody)
        {

            try
            {
                //var json = request.Content.ReadAsStringAsync().Result.Replace("\\\"","\"");
                var obj = JObject.Parse(jsonbody.ToString());
                var applicationUser = new ApplicationUser()
                {
                    UserName = (string)obj["Email"],
                    Email = (string)obj["Email"],
                    Name = (string)obj["Name"],
                    IsApproved = false
                };


                var result = _applicationUserManager.Create(applicationUser, (string)obj["Password"]);
                if (result.Succeeded)
                {
                    await _applicationUserManager.AddToRoleAsync(applicationUser.Id, "Patient");
                }
                return result.Succeeded ? Ok().ToString() : this.BadRequest().ToString();
            }
            catch (Exception ex)
            {
                return this.BadRequest().ToString();
            }
        }


        [HttpPost]
        public async Task<string> LoginUser([FromBody]JToken jsonbody)
        {
            try
            {
                var obj = JObject.Parse(jsonbody.ToString());
                var email = (string) obj["Login"];
                var pass = (string)obj["Password"];
                var result = await _signInManager.PasswordSignInAsync(email,pass ,true);
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}