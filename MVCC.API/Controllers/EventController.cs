﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;

namespace MVCC.API.Controllers
{
    public class EventController : ApiController
    {
        private readonly EventRepository _eventRepository;

        public EventController(EventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        [HttpGet]
        public JsonResult<List<Event>> GetEvents()
        {
            try
            {
                var list = _eventRepository.GetEventsByMonth(DateTime.Today.Month);
                return Json(list.ToList());
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
