﻿// -----------------------------------------------------------------------
// <copyright file="ErrorController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Web;
using System.Web.Http;
using Elmah;

namespace MVCC.API.Controllers
{
    #region Usings

    

    #endregion Usings

    public class ErrorController : ApiController
    {
        [System.Web.Mvc.AllowAnonymous]
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.HttpPut]
        [System.Web.Mvc.HttpDelete]
        [System.Web.Mvc.HttpHead]
        [System.Web.Mvc.HttpOptions]
        public IHttpActionResult NotFound(string path)
        {
            // log error to ELMAH
            ErrorSignal.FromCurrentContext().Raise(new HttpException(404, "404 Not Found: /" + path));

            // return 404
            return this.NotFound();
        }
    }
}