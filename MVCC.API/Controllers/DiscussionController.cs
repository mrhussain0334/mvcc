﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;
using Newtonsoft.Json.Linq;
using PagedList;

namespace MVCC.API.Controllers
{
    public class DiscussionController : ApiController
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly DiscussionRepository _discussionRepository;
        private readonly CommentRepository _commentRepository;

        public DiscussionController(CategoryRepository categoryRepository, DiscussionRepository discussionRepository,
            CommentRepository commentRepository)
        {
            _categoryRepository = categoryRepository;
            _discussionRepository = discussionRepository;
            _commentRepository = commentRepository;
        }



        [HttpGet]
        public JsonResult<List<Category>> GetCategories()
        {
            var list = _categoryRepository.GetAll().Select(category => new Category()
            {
                Id = category.Id,
                Name = category.Name
            }).ToList();
            return Json(list);
        }


        [HttpGet]
        public JsonResult<List<Discussion>> GetDiscussionByPatient([FromUri] string id, [FromUri] int page = 1)
        {
            try
            {
                var list = _discussionRepository.GetByUser(Guid.Parse(id)).ToPagedList(page, 20);
                return Json(list.ToList());

            }
            catch (Exception)
            {
                return null;
            }
        }
        [HttpGet]
        public JsonResult<List<Discussion>> GetDiscussion([FromUri] string id, [FromUri] int page = 1)
        {
            try
            {
                var list = _discussionRepository.GetByCategory(Guid.Parse(id)).ToPagedList(page, 20);

                return Json(list.ToList());

            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpDelete]
        public IHttpActionResult DeleteDiscussion([FromUri] string id)
        {
            try
            {
                if (_discussionRepository.Delete(Guid.Parse(id)))
                {
                    return Ok();
                }
                return BadRequest();

            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpPost]
        public IHttpActionResult CreateComment([FromBody] JToken jsonbody)
        {
            try
            {
                var obj = JObject.Parse(jsonbody.ToString());
                var comment = new Comment()
                {
                    CommentedDate = DateTime.Now,
                    CommenterId = Guid.Parse(obj["CommenterId"].ToString()),
                    DiscussionId = Guid.Parse(obj["DiscussionId"].ToString()),
                    Context = obj["Context"].ToString()
                };
                if (_commentRepository.Add(comment) != null)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public IHttpActionResult UpdateDiscussion([FromBody] JToken jsonbody)
        {
            try
            {
                var obj = JObject.Parse(jsonbody.ToString());
                var dicussion = new Discussion()
                {
                    CategoryId = Guid.Parse(obj["CategoryId"].ToString()),
                    Id = Guid.Parse(obj["Id"].ToString()),
                    Name = obj["Name"].ToString(),
                    Description = obj["Description"].ToString(),
                };
                if (_discussionRepository.Update(dicussion))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public IHttpActionResult CreateDiscussion([FromBody] JToken jsonbody)
        {
            try
            {
                var obj = JObject.Parse(jsonbody.ToString());
                var dicussion = new Discussion()
                {
                    DateCreated = DateTime.Now,
                    CreaterId = Guid.Parse(obj["CreaterId"].ToString()),
                    CategoryId = Guid.Parse(obj["CategoryId"].ToString()),
                    Name = obj["Name"].ToString(),
                    Description = obj["Description"].ToString(),
                    DateUpdated = DateTime.Now
                };
                if (_discussionRepository.Add(dicussion) != null)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public JsonResult<List<Comment>> GetComment([FromUri] string id, [FromUri] int page = 1)
        {
            try
            {
                var list = _commentRepository.GetByDiscussion(Guid.Parse(id)).ToPagedList(page, 20);

                return Json(list.ToList());

            }
            catch (Exception)
            {
                return null;
            }
        }


    }

}
