﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Infrastructure;
using MVCC.Model.Entities;

namespace MVCC.API.Controllers
{
    public abstract class ApiHubController<T> : Controller
        where T : Hub
    {
        private readonly IHubContext hub;
        public IHubConnectionContext<dynamic> Clients { get; set; }
        public IGroupManager GroupManager { get; set; }

        protected ApiHubController(IConnectionManager signalRConnectionManager)
        {
            hub = signalRConnectionManager.GetHubContext<T>();
            Clients = hub.Clients;
            GroupManager = hub.Groups;
        }
    }
}