﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;
using Newtonsoft.Json.Linq;

namespace MVCC.API.Controllers
{
    public class AppointmentController : ApiController
    {
        private readonly AppointmentRepository _appointmentRepository;

        public AppointmentController(AppointmentRepository appointmentRepository)
        {
            _appointmentRepository = appointmentRepository;
        }

        public JsonResult<List<Appointment>> GetAppointment([FromUri] string id, [FromUri] int? page)
        {
            try
            {
                var Page = page ?? 1;
                var list = _appointmentRepository.GetByPatient(Guid.Parse(id), Page);
                return Json(list.ToList());
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IHttpActionResult CreateAppointment([FromBody] JToken jsonBody)
        {
            try
            {
                var obj = JObject.Parse(jsonBody.ToString());
                var appointment = obj.ToObject<Appointment>();
                appointment.AcceptedDate = appointment.AppointmentDate;
                appointment.IsApproved = false;
                var x = _appointmentRepository.Add(appointment);
                if (x != null)
                {
                   return Ok();
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
