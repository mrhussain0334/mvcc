﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;
using Newtonsoft.Json.Linq;

namespace MVCC.API.Controllers
{
    public class MessageController : ApiController
    {

        private readonly MessageRepository _messageRepository;

        public MessageController(MessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }




        [HttpGet]
        public JsonResult<List<Message>> GetMessage([FromUri] string id, [FromUri] string did)
        {
            try
            {
                var list = _messageRepository.GetMessages(Guid.Parse(id), Guid.Parse(did), 20).ToList();
                return Json(list);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public IHttpActionResult CreateMessage([FromBody] JToken jsonbody)
        {
            try
            {
                var obj = JObject.Parse(jsonbody.ToString());
                var message = new Message()
                {
                    FromId = Guid.Parse(obj["FromId"].ToString()),
                    ToId = Guid.Parse(obj["ToId"].ToString()),
                    Context = obj["Context"].ToString(),
                    DateSent = DateTime.Now,
                    DateRead = DateTime.Now,
                    DateRecieved = DateTime.Now
                };
                if (_messageRepository.Add(message) != null)
                    return Ok();
                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }






    }
}