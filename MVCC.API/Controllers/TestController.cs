﻿// -----------------------------------------------------------------------
// <copyright file="TestController.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Web.Http;
using System.Web.Http.Results;
using MVCC.Data.Repositories;

namespace MVCC.API.Controllers
{
    #region Usings

    using System.Collections.Generic;
    using Model.Entities;

    #endregion Usings

    public class TestController : ApiController
    {
        private readonly TestRepository _testRepository;

        public TestController(TestRepository testRepository)
        {
            _testRepository = testRepository;
        }

        [HttpGet]
        public JsonResult<IEnumerable<Test>> GetTests([FromUri] string id)
        {
            try
            {
                var list = _testRepository.GetTestByPatient(Guid.Parse(id));
                return Json(list);
            }
            catch (Exception)
            {
                return null;
            }
        }

        
    }
}