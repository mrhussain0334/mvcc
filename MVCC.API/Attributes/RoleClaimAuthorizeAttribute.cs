﻿// -----------------------------------------------------------------------
// <copyright file="RoleClaimAuthorizeAttribute.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.API.Attributes
{
    #region Usings

    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using Model.Entities;
    using Web.Shared.Extensions;

    #endregion Usings

    public class RoleClaimAuthorizeAttribute : AuthorizeAttribute
    {
        public RoleClaim RoleClaim { get; set; }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!actionContext.RequestContext.Principal.HasRoleClaim(this.RoleClaim))
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
        }
    }
}