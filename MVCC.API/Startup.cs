﻿// -----------------------------------------------------------------------
// <copyright file="Startup.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

#region Usings

using Microsoft.Owin;
using MVCC.API;

#endregion Usings

[assembly: OwinStartup("API", typeof(Startup))]

namespace MVCC.API
{
    #region Usings

    using Owin;

    #endregion Usings

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureAuth(app);
        }
    }
}