﻿// -----------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using MVCC.API.App_Start;

namespace MVCC.API
{
    #region Usings

    using System.Linq;
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Http.ExceptionHandling;
    using Elmah.Contrib.WebApi;
    using Microsoft.Owin.Security.OAuth;

    #endregion Usings

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // enable elmah
            //config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());

            // Web API configuration and services Configure Web API to use only bearer token authentication.
            //config.SuppressDefaultHostAuthentication();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{action}/{id}",
                new { id = RouteParameter.Optional}
                
                );
            //new { controller = GetControllerNames() }
            //config.Filters.Add(new AuthorizeAttribute());

            // catch all route mapped to ErrorController so 404 errors can be logged in elmah
            config.Routes.MapHttpRoute("NotFound", "{*path}", new { controller = "Error", action = "NotFound" });
        }

        // helper method that returns a string of all api controller names in this solution, to be
        // used in route constraints above
        private static string GetControllerNames()
        {
            var controllerNames =
                Assembly.GetCallingAssembly()
                    .GetTypes()
                    .Where(
                        x =>
                            x.IsSubclassOf(typeof(ApiController)) &&
                            x.FullName.StartsWith(
                                MethodBase.GetCurrentMethod().DeclaringType.Namespace + ".Controllers"))
                    .ToList()
                    .Select(x => x.Name.Replace("Controller", ""));

            return string.Join("|", controllerNames);
        }
    }
}