﻿// -----------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Web.Http;

namespace MVCC.API
{
    #region Usings

    using System.Web.Mvc;
    using System.Web.Routing;

    #endregion Usings

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapHttpRoute(
                name: "MyApi",
                routeTemplate: "api/{controller}/{action}/");
             
            routes.MapRoute("Default", "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional});
        }
    }
}