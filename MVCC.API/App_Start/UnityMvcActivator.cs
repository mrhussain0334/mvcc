// -----------------------------------------------------------------------
// <copyright file="UnityMvcActivator.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

#region Usings

using MVCC.API.App_Start;
using WebActivatorEx;

#endregion Usings

[assembly: PreApplicationStartMethod(typeof(UnityMvcActivator), "Start")]
[assembly: ApplicationShutdownMethod(typeof(UnityMvcActivator), "Shutdown")]

namespace MVCC.API.App_Start
{
    #region Usings

    using System.Linq;
    using System.Web.Http;
    using System.Web.Mvc;
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.Mvc;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    #endregion Usings

    /// <summary>
    /// Provides the bootstrapping for integrating Unity with ASP.NET MVC.
    /// </summary>
    public static class UnityMvcActivator
    {
        /// <summary>
        /// Disposes the Unity container when the application is shut down.
        /// </summary>
        public static void Shutdown()
        {
            IUnityContainer container = UnityConfig.GetConfiguredContainer();
            container.Dispose();
        }

        /// <summary>
        /// Integrates Unity when the application starts.
        /// </summary>
        public static void Start()
        {
            IUnityContainer container = UnityConfig.GetConfiguredContainer();

            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(container));

            DependencyResolver.SetResolver(new Microsoft.Practices.Unity.Mvc.UnityDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

            // TODO: Uncomment if you want to use PerRequestLifetimeManager
            DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
        }
    }
}