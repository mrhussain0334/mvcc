// -----------------------------------------------------------------------
// <copyright file="UnityConfig.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using MVCC.Data.Infrastructure;

namespace MVCC.Api
{
    using System;
    using System.Web;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Microsoft.Practices.Unity;
    using MVCC.Data;
    using MVCC.Data.Stores;
    using MVCC.Model.Entities;

    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container

        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }

        #endregion Unity Container

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or API controllers
        /// (unless you want to change the defaults), as Unity allows resolving a concrete type even
        /// if it was not previously registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a
            //       Microsoft.Practices.Unity.Configuration to the using statements. container.LoadConfiguration();

            // TODO: Register your types here container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterTypes(
                AllClasses.FromLoadedAssemblies(),
                WithMappings.FromMatchingInterface,
                WithName.Default,
                WithLifetime.Custom<PerRequestLifetimeManager>);


            container.RegisterType<ApplicationContext, ApplicationContext>(new PerRequestLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser, Guid>, ApplicationUserStore>(new PerRequestLifetimeManager());
            container.RegisterType<IRoleStore<ApplicationRole, Guid>, ApplicationRoleStore>(new PerRequestLifetimeManager());
            container.RegisterType(typeof(IRepository<>), typeof(RepositoryBase<>));
            container.RegisterType<IAuthenticationManager>(new PerRequestLifetimeManager(),
                new InjectionFactory(
                    o => HttpContext.Current.GetOwinContext().Authentication));
        }
    }
}