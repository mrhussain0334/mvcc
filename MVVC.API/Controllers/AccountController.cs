﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using MVCC.Data.Repositories;
using MVCC.Model.Entities;
using MVCC.Service.Managers;
using MVVC.API.Providers;

namespace MVVC.API.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        
        public AccountController(DiscussionRepository discussionRepository, ApplicationUserManager userManager)
        {
            _discussionRepository = discussionRepository;
            _userManager = userManager;
        }

        private readonly DiscussionRepository _discussionRepository;

        [Route("Test")]
        public IEnumerable<Discussion> GetUser()
        {
            return _discussionRepository.GetAll().ToList();
        }
        

        
    }
}
