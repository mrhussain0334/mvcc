﻿// -----------------------------------------------------------------------
// <copyright file="HomeControllerTest.cs" company="Mount Vernon Cancer Centre">
//     Copyright (c) Mount Vernon Cancer Centre. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MVCC.Website.Tests.Controllers
{
    #region Usings

    using System.Web.Mvc;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Website.Controllers;

    #endregion Usings

    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ////ViewResult result = controller.About() as ViewResult;

            // Assert
            ////Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ////ViewResult result = controller.Contact() as ViewResult;

            // Assert
            ////Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}